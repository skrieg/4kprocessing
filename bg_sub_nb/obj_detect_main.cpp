/**
 * @brief Main function definition
 * @file obj_detect_main.cpp
 * @author sylvain.krieg@master.hes-so.ch
 * @date decembre 2015
 * @version 0.2.3
 */

#include "obj_detect.h"
#include "Log.h"
#include <pthread.h>
#include <thread>

using namespace cv;
using namespace std;

/** Function Headers */
void help();

//vector<Mat> vCaptureResult;
//vector<Mat> vPreprocResult;
//vector<Mat> vSegmResult;
//vector<Mat> vBgSubResult;

void help() {
    cout
            << "--------------------------------------------------------------------------" << endl
            << "This program makes the classification between pedestrians and electric forklift" << endl
            << "You can process videos:" << endl
            << "A: without arguments, it takes the video file specified in obj_detect_main file" << endl
            << "B: with 1 argument, the name of the video, the path is specified in obj_detect_main_file" << endl
            << "C: with 2 arguments, the first is the video path, the second the log path " << endl
            << "Usage:" << endl
            << "for example: ./bg_sub_nb chariot1" << endl
            << "or: ./bg_sub_nb /home/user/vid/cam02/xyz.avi  /home/user/xyz.log" << endl
            << "--------------------------------------------------------------------------" << endl
            << endl;
}
//Set the user path
//#define PC_PATH  "/home/sylvain"
#define PC_PATH "/home/skrieg"

//Set the video directory
//#define VID_PATH "/vid/cam02/chariots/" //Chariots
//#define VID_PATH "/vid/cam02/pietons/" //Pietons
#define VID_PATH "/vid/cam02/"

//Set the video extension
#define VID_EXTENSION ".avi"

//Set the video name
//#define VID_NAME "20130723_001452_s01c02"
//#define VID_NAME "20130725_005208_s01c02"
//#define VID_NAME "20130725_005546_s01c02"
//#define VID_NAME "20130725_005712_s01c02"
//#define VID_NAME "20130725_010108_s01c02"
//Pietons
//#define VID_PATH "/vid/cam02/pietons/"
//#define VID_NAME "20130725_011100_s01c02"
//#define VID_NAME "20130725_051104_s01c02"
//#define VID_NAME "20130725_012953_s01c02"
//#define VID_NAME "20130725_013134_s01c02"
//#define VID_NAME "20130720_234709_s01c02"
#define VID_NAME "chariot1"
//#define VID_NAME "chariot2"
//#define VID_NAME "chariot3"
//#define VID_NAME "chariot4"
//#define VID_NAME "chariot5"
//#define VID_NAME "pieton1"
//#define VID_NAME "pieton2"
//#define VID_NAME "pieton3"
//#define VID_NAME "pieton4"
//#define VID_NAME "pieton5"
#define VID_NAME "japan4K"
//#define VID_NAME "Dahua4K"
#define VID_NAME "Bosch4K"

#define VIDEO_PATH PC_PATH VID_PATH VID_NAME VID_EXTENSION
#define LOG_PATH PC_PATH "/log/" VID_NAME ".log"

/**
 * @function main
 */
int main(int argc, char* argv[]) {
    help();

    const char* videoFileName; // = VIDEO_PATH;   
    const char* logPath; // = LOG_PATH;

    std::string tmpString;
    switch (argc) {
        case 1: //no parameters
            videoFileName = PC_PATH VID_PATH VID_NAME VID_EXTENSION;
            logPath = LOG_PATH;
            cout << "A: param: argv[0] " << argv[0] << endl;
            break;
        case 2: //parameter is the video name
            cout << "B: param: argv[0] " << argv[0] << " argv[1]" << argv[1] << endl;
            tmpString = string(PC_PATH) + string(VID_PATH) + string(argv[1]) + string(VID_EXTENSION);
            videoFileName = (char*) tmpString.c_str();
            tmpString = string(PC_PATH) + "/log/" + string(argv[1]) + ".log";
            logPath = (char*) tmpString.c_str();
            break;
        case 3: //parameters are the video path and the log path
            cout << "C: param: argv[0] " << argv[0] << " argv[1]" << argv[1] << " argv[2]" << argv[2] << endl;
            videoFileName = (char*) string(argv[1]).c_str();
            logPath = (char*) string(argv[2]).c_str();
            // logPath = (char*) string(argv[2]);
            break;
        default:
            cerr << "Incorret input list" << endl;
            cerr << "exiting..." << endl;
            return EXIT_FAILURE;
    }
//    cout << "----------------------------------------------------"<<endl;
//    cout << "video path: |" << videoFileName << "|" << endl;
    cout << "log path: "<<logPath<<endl;


    
    //cuda_bgsub();
    ffps(videoFileName, logPath);
    //monoTask(videoFileName, logPath);
    //videoFileName="/home/skrieg/vid/jpn/japan4K_div1_q21.mp4";
    //multiTask(videoFileName, logPath);
    //destroy GUI windows
    destroyAllWindows();
    return EXIT_SUCCESS;
}

VideoCapture openVid(string arg) {
    cout << "Open video file: " << arg << endl;
    VideoCapture capture(arg); //sk
    cout << "video opened " << arg << endl;

    //VideoCapture capture(videoFilename);
    if (!capture.isOpened()) {
        capture.open(atoi(arg.c_str())); //sk
        capture.set(CAP_PROP_FPS, 15);
        capture.set(CAP_PROP_FRAME_WIDTH, 800);
        capture.set(CAP_PROP_FRAME_HEIGHT, 600);
        //    capture.set(CAP_PROP_FRAME_WIDTH, 1600);
        //    capture.set(CAP_PROP_FRAME_HEIGHT, 1200);
        //error in opening the video input
        //cerr << "Unable to open video file: " << videoFilename << endl;
        //exit(EXIT_FAILURE);
    }
    if (!capture.isOpened()) {
        cerr << "Failed to open the video device, video file or image sequence!\n" << endl;
        cerr << arg;
        exit(EXIT_FAILURE);
    }
    return capture;
}

VideoCapture openVid(const char* videoFilename) {
    cout << "Open video file: " << videoFilename << "|" << endl;
    string arg = videoFilename; //sk
    //arg = "/home/skrieg/vid/cam02/chariots/20130723_001452_s01c02.avi";
    VideoCapture capture(arg); //sk
    //  waitKey(5);
    cout << "video opened " << videoFilename << endl;

    //VideoCapture capture(videoFilename);
    if (!capture.isOpened()) {
        capture.open(atoi(arg.c_str())); //sk
        capture.set(CAP_PROP_FPS, 15);
        capture.set(CAP_PROP_FRAME_WIDTH, 800);
        capture.set(CAP_PROP_FRAME_HEIGHT, 600);
        //    capture.set(CAP_PROP_FRAME_WIDTH, 1600);
        //    capture.set(CAP_PROP_FRAME_HEIGHT, 1200);
        //error in opening the video input
        //cerr << "Unable to open video file: " << videoFilename << endl;
        //exit(EXIT_FAILURE);
    }
    if (!capture.isOpened()) {
        cerr << "Failed to open the video device, video file or image sequence!\n" << endl;
        cerr << videoFilename;
        exit(EXIT_FAILURE);
    }
    return capture;
}


