/* 
 * File:   obj_detect.h
 * Author: sylvain
 *
 * Created on October 19, 2015, 2:29 PM
 */

#ifndef OBJ_DETECT_H
#define	OBJ_DETECT_H


#include "opencv2/videoio/videoio.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/video/video.hpp"

//C
#include <stdio.h>
//C++
#include <iostream>
#include <sstream>
#include <list> 

#include <time.h> // to calculate time needed
#include <unistd.h>    //for function usleep
#include <iomanip> //setfill, setw

//blob detection
#include "features2d.hpp"


#include <time.h> // to calculate time needed
#include <unistd.h>    //for function usleep
#include <iomanip> //setfill, setw

//blob detection
//#include "features2d.hpp"

#include <cstdio>
#include <iostream>
#include <sstream>

//Shared memory with process
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <fcntl.h>

//Shared memory with process
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/wait.h>
#include <sys/mman.h>
#include <pthread.h>

#include <mutex>

#include "Log.h"

#include <fstream>


using namespace cv;
using namespace std;

VideoCapture openVid(const char* videoFilename);
VideoCapture openVid(string videoFilename);
void detectionParamsInit();


void pre_processing(VideoCapture captureIN, String windowName);


//#define PATH_MAX 100


#define SRC_RAW 0
#define SRC_PREPROC 1
#define SRC_SEGMNT 2
#define SRC_DETECT 3
#define SRC_HOUGH 4
#define SRC_RESULT 5
#define EROSION_SIZE 1

#define DO_NOTHING 0
#define DO_MOG  1
#define DO_CONTOUR 2

#define MS *1000

#define RING_BUFFER_SIZE 4

#define AVG_FPS_SIZE 10
#define MAT_MEMORY_SIZE 6220800 
#define RESIZE_SIZE Size(640, 480)
#define LOW_RES_SIZE Size(640, 480)

#define LINE_THICKNESS 1

struct imgRingBuffer {
    int wrIndex;
    int rdIndex;
    pthread_mutex_t mtx;
    char* dataImageschar;
};

////////////////////////////////////////////////////////////////////
// toolBox
// 
////////////////////////////////////////////////////////////////////
void printVidInfo(VideoCapture capt);

////////////////////////////////////////////////////////////////////
// GUI
// File obj_detect_gui.cpp
////////////////////////////////////////////////////////////////////

void cb_void(int p1, void* p2);
void cb_shadow_on(int p1, void* p2);
void cb_shadow_off(int p1, void* p2);
void cb_rad_srcRaw(int p1, void* p2);
void cb_rad_srcOp(int p1, void* p2);
void cb_rad_SrcResult(int p1, void* p2);
void cb_dilate_on(int p1, void* p2);
void cb_dilate_off(int p1, void* p2);
void cb_open_on(int p1, void* p2);
void cb_open_off(int p1, void* p2);
void cb_blob_det_on(int p1, void* p2);
void cb_blob_det_off(int p1, void* p2);

/**
 * Gui parameters
 */
typedef struct GuiParam {
    int Mog2Variance_trkbar;
    int Mog2Ratio_trkbar;
    int mixture_trkbar;
    int Mog2Complexity_trkbar;
    int ShadowActive;
    int shadow_trkbar;
    int history_trkbar;
    int KnnDist2Threshold;
    int kNNSamples;
    int DilateActive;
    int dilateElmt_trkbar;
    int openActive;
    int opnElmt_trkbar;
    int closeActive;
    int closElmt_trkbar;
    int blobSizeMin_trkbar; //not in gui, unused
    int blobSizeMax_trkbar; //not in gui, unused
    int blobDetectActive; //not in gui, unused
    int objSizeMin_trkbar;
    int contPoly_trkbar;
    int canny_t1;
    int canny_t2;
    int hough_line_min;
    int hough_accu_tresh;
    int hough_max_gap;
    int gridFillActive;
    int test1_trkbar;
    int holesFillActive;
    GuiParam(void);

} GuiParam, *pGuiParam;

void blob_detector_config(GuiParam* p);
void createGUI(string windowName, GuiParam* param);

vector<KeyPoint> blob_detection(Mat img, GuiParam* p);




void fill_img_with_grid(Mat& imgOUT, Mat& gridIN);
int fill_grid(Mat& imgIN, Mat& gridINOUT);
bool check_grid_dividers(Mat& imgIN, int divWidth, int divHeight);
int grid_fill_holes(Mat& fgClean_, Mat& fgGrid_, int threshold, int color);


void ffps(const char* videoFilename, const char* logPath);
void monoTask(const char* videoFilename, const char* logPath);
void multiTask(const char *videoFilename, const char* logPath);

//background subtraction
Mat backGroundSubMog2(GuiParam param, cv::Mat &frame, cv::Mat &retFrame, BackgroundSubtractorMOG2* pSub, int pause);
Mat backGroundSubKNN(GuiParam param, cv::Mat &frame, cv::Mat &retFrame, BackgroundSubtractorKNN* pSub, int pause);


void obj_detection(const Mat& sourceImage, Mat& detectionFrame);

//fill holes test
void fill_holes_test(const char* videoFilename, const char* logPath);

//segmentation

//detection
void detect_contours(GuiParam param, const Mat fgFrame, const Mat colorFrameIn, Mat detectionColorMask, vector<vector<Point> >& actorsContour, vector<Rect> & actorsRectangle);

//classification
void annoteFrame(Mat frame, Mat& annotedFrame, vector<Rect> & objRectangles, vector<int>& prevScore);
void actors_classification_v3(Mat detectionMask, Mat& houghFrame, GuiParam param, int paused, vector<Rect> & objRectangles, vector<int>& prevScore, vector<int>& actuScore);
void actors_classification_v2(GuiParam param, int paused, Mat detectionMask,  Mat& houghFrame, Mat& outMat, /*vector<vector<Point> >& actorsContour, */vector<Rect> & objRectangles, vector<Rect>& prevRectangles, vector<Rect>& actuRectangles, vector<int>& prevScore, vector<int>& actuScore);
void actors_classification(GuiParam param, int paused, Mat& frame, Mat detectionMask, Mat& outMat, vector<vector<Point> >& retCont, vector<Rect> & objRectangles, vector<Rect>& prevRectangles, vector<Rect>& actuRectangles, vector<int>& prevScore, vector<int>& actuScore);
#define LENGTH_CORRECTION 10.0
#define INCREASE_FACTOR 10
#define DECREASE_FACTOR 2
#define SCORE_THRESHOLD_MACHINE 60
#define SCORE_THRESHOLD_HUMAN -40
#define UNKNOWN_COLOR Scalar(255, 255, 255) // white
#define HUMAN_COLOR Scalar(0, 0, 255); //red
#define MACHINE_COLOR Scalar(255, 255, 0) //blue





#endif	/* OBJ_DETECT_H */

