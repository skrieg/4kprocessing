/**
 * @brief Object classification
 * @file classification.cpp
 * @author sylvain.krieg@master.hes-so.ch
 * @date decembre 2015
 */

#include "obj_detect.h"

/**
 * Return the gravity center of the rectangle
 * @param obj the input rectangle
 * @return center of gravity
 */
Point getRectCentroid(Rect obj) {
    return Point(obj.tl().x + (obj.br().x - obj.tl().x) / 2, obj.tl().y + (obj.br().y - obj.tl().y) / 2);
}

/**
 * Return true if the point is inside the rectangle
 * @param objCentroid
 * @param boundingRect
 * @return true if the point is bouding the rectangle, false otherwise.
 */
bool isBounding(Point objCentroid, Rect boundingRect) {
    if ((objCentroid.x <= boundingRect.br().x && objCentroid.x >= boundingRect.tl().x)
            && (objCentroid.y <= boundingRect.br().y && objCentroid.y >= boundingRect.tl().y)) {//point is bounded by rectangle
        return true;
    } else
        return false;
}

/**
 * Verify if the rectangle center is inside the other rectangle
 * @param prevRectangles parent rectangle
 * @param newObj rectangle to be tested if it is bounding the other
 * @return true if bounding
 */
bool isPresent(vector<Rect>& prevRectangles, Rect newObj) {
    size_t vectSize = prevRectangles.size();
    for (size_t i = 0; i < vectSize; i++) {
        if (isBounding(getRectCentroid(newObj), prevRectangles[i])) {
            return true; //the new obj is bounding a rectangle in the list
        }
    }
    return false;
}

/**
 * Return the position in the vector
 * @param prevRectangles
 * @param newObj
 * @return 
 */
int getPos(vector<Rect>& prevRectangles, Rect newObj) {
    size_t vectSize = prevRectangles.size();
    for (size_t i = 0; i < vectSize; i++) {
        if (isBounding(getRectCentroid(newObj), prevRectangles[i])) {
            return i; //the new obj is bounding a rectangle in the list
        }
    }
    return -1;
}

/**
 * 
 * @param nbLines number to be added
 * @param score previous score
 * @return new score
 */
int compute_score(int nbLines, int score) {
    score = (score + nbLines * INCREASE_FACTOR) - DECREASE_FACTOR;
    return score;
}

/**
 * Simple actors classification. Can only handle ONE object, multiple objects for future development.
 * @param frame video RGB frame
 * @param detectionMask color mask from actors
 * @param outMat video RGB with obj informations
 * @param retCont detected contours 
 * @param objRectangles
 */
void actors_classification(GuiParam param, int paused, Mat& frame, Mat detectionMask, Mat& outMat, vector<vector<Point> >& retCont, vector<Rect> & objRectangles, vector<Rect>& prevRectangles, vector<Rect>& actuRectangles, vector<int>& prevScore, vector<int>& actuScore) {
    Mat actorsBW;
    cvtColor(detectionMask, actorsBW, CV_BGR2GRAY);

    //Canny detector
    Mat cannyMat;
    cv::Canny(actorsBW, cannyMat/*outMat*/, param.canny_t1, param.canny_t2, 3, false);

    //Sobel filter
    //    Mat sobelMat, sobelBW;
    //    int scale = 1;
    //    cv::Sobel(actorsColor, sobelMat, -1/*CV_16S*/, 0, 1, 5, scale, BORDER_DEFAULT);
    //    //imshow("sobel", sobelMat);
    //    cvtColor(sobelMat, sobelBW, CV_BGR2GRAY);
    //    cv::threshold(sobelBW, sobelBW, 125, 255, cv::THRESH_BINARY);
    //    imshow("sobel", sobelBW);

    //Hough
    Mat houghFrame;
    cvtColor(cannyMat, houghFrame, COLOR_GRAY2BGR); //
    vector<Vec4i> lines;

    HoughLinesP(cannyMat, lines, 1, CV_PI / 180, param.hough_accu_tresh, param.hough_line_min, param.hough_max_gap); //1, CV_PI / 180, 10, 1, 1);
    for (size_t i = 0; i < lines.size(); i++) {
        line(houghFrame, Point(lines[i][0], lines[i][1]),
                Point(lines[i][2], lines[i][3]), Scalar(0, 255, 0), LINE_THICKNESS + 1, 8);
    }
    imshow("hough lines", houghFrame);

    //Print actors informations on the color frame
    outMat = frame.clone();
    //write the object number

    //For each actor in the scene
    Mat objMat;
    int nbObj = retCont.size();
    int nbPrevObjects = prevRectangles.size();
    Point newObjCentroid;
    /* How it works:
     * For all the objects in the new frame, check if near a previous object (center of the boundingRectangle of the new object is inside the 
     * boundingRectangle of a previous object).     
     */
    for (size_t i = 0; i < nbObj; i++) { //for all objects in the new frame
        newObjCentroid = Point(objRectangles[i].tl().x + (objRectangles[i].br().x - objRectangles[i].tl().x) / 2, objRectangles[i].tl().y + (objRectangles[i].br().y - objRectangles[i].tl().y) / 2);
        if (isPresent(prevRectangles, objRectangles[i])) { //if present in the previous frame
            //cout << "present: " << getPos(prevRectangles, objRectangles[i]) << endl;
            actuRectangles.push_back(objRectangles[i]);

            //HoughLines inside the rectangle
            Mat subFrame = cannyMat(objRectangles[i]);
            vector<Vec4i> lineSub;
            HoughLinesP(subFrame, lineSub, 1, CV_PI / 180, param.hough_accu_tresh, param.hough_line_min, param.hough_max_gap); //1, CV_PI / 180, 10, 1, 1);

            int score;
            if (paused)
                score = prevScore[getPos(prevRectangles, objRectangles[i])];
            else
                score = compute_score(lineSub.size(), prevScore[getPos(prevRectangles, objRectangles[i])]);

            actuScore.push_back(score);
            cout << "PROB:" << actuScore.back() << "..." << score << " nb lines:" << lineSub.size() << endl;
            //actuProba.push_back(compute_probability(lines.size() , prevProba[getPos(prevRectangles, objRectangles[i])]) );//take the previous proba value
            Scalar rectColor;
            if (score > 10)
                rectColor = Scalar(255, 0, 0); //transpalettes
            else if (score<-10)
                rectColor = Scalar(0, 255, 255); //humain
            else
                rectColor = Scalar(255, 255, 255); //??
            rectangle(outMat, objRectangles[i].tl(), objRectangles[i].br(), rectColor, 2, 8, 0); //draw rectangle on image

        } else { //new object
            //cout << "not present>>ADD" << endl;
            actuRectangles.push_back(objRectangles[i]);
            actuScore.push_back(0);

        }
        putText(outMat, std::to_string(i) + ":" + std::to_string(lines.size()), cv::Point(retCont.back()[0].x, retCont.back()[0].y), FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 255));
        retCont.pop_back();
        objRectangles.pop_back();
    }
    prevRectangles.clear();
    prevRectangles.swap(actuRectangles);
    actuRectangles.clear();

    prevScore.clear();
    prevScore.swap(actuScore);
    actuScore.clear();
}

/**
 * Actors classification usin the distance to adjust the houghlines minimum length. Can only handle ONE object, multiple objects for future development.
 * @param param parameters structure GuiParam
 * @param frame input fram
 * @param detectionMask input frame, mask with detected objets
 * @param outMat output result
 * @param actorsContour detected contours 
 * @param objRectangles bounding rectangle arround the detected actors
 * @param prevRectangles for tracking, contains previous bounding rectangles
 * @param actuRectangles for tracking, new rectangles
 * @param prevScore old classification score
 * @param actuScore new classification score
 */
void actors_classification_v2(GuiParam param, int paused, Mat detectionMask, Mat& houghFrame, Mat& outMat, /*vector<vector<Point> >& actorsContour,*/ vector<Rect> & objRectangles, vector<Rect>& prevRectangles, vector<Rect>& actuRectangles, vector<int>& prevScore, vector<int>& actuScore) {
    //Number of objects
    //cout <<"nb obj:"<<actorsContour.size()<<" "<<objRectangles.size()<< " "<<prevRectangles.size()<<endl;
    int nbObj = objRectangles.size();
    //Compute y position
    //int nbObj = actorsContour.size();
    Point newObjCentroid;
    for (size_t i = 0; i < nbObj; i++) { //for all objects in the new frame
        newObjCentroid = getRectCentroid(objRectangles[i]);
    }

    //compute line ratio
    double lineMin = param.hough_line_min + (newObjCentroid.y / LENGTH_CORRECTION);

    Mat actorsBW; //convert to black&white
    cvtColor(detectionMask, actorsBW, CV_BGR2GRAY);

    //Canny detector
    Mat cannyMat;
    cv::Canny(actorsBW, cannyMat/*outMat*/, param.canny_t1, param.canny_t2, 3, false);

    //Mat houghFrame;
    cvtColor(cannyMat, houghFrame, COLOR_GRAY2BGR); //
    vector<Vec4i> lines;

    //Hough lines detector then draw the lines
    HoughLinesP(cannyMat, lines, 1, CV_PI / 180, param.hough_accu_tresh, lineMin/*param.hough_line_min*/, param.hough_max_gap); //1, CV_PI / 180, 10, 1, 1);
    for (size_t i = 0; i < lines.size(); i++) {
        line(houghFrame, Point(lines[i][0], lines[i][1]),
                Point(lines[i][2], lines[i][3]), Scalar(0, 255, 0), LINE_THICKNESS + 1, 8);
    }

    //imshow("hough lines", houghFrame);

    //Compute score
    if (nbObj /*actorsContour.size()*/ > 0) {
        //Compute score
        int score;
        if (paused)
            score = prevScore[0];
        else
            score = compute_score(lines.size(), prevScore[0]);
        //logStream << lines.size() << " " << newObjCentroid.y; //write log
        //cout << "line: " << param.hough_line_min << " adjust: " << lineMin << " posY: " << newObjCentroid.y << " score: " << score << endl;
        cout << "nbLines: " << lines.size() << " score: " << score << endl;
        actuScore.push_back(score);
    }

    //Draw bounding rectangles
    if (actuScore.size() > 0) {
        Scalar rectColor;
        if (actuScore[0] > 60) {
            cout << "chariot" << endl;
            rectColor = MACHINE_COLOR;
        } else if (actuScore[0] < -40) {
            cout << "pieton" << endl;
            rectColor = HUMAN_COLOR;
        } else {
            cout << "unknown" << endl;
            rectColor = UNKNOWN_COLOR;
        }

        outMat = houghFrame.clone();
        for (size_t i = 0; i < objRectangles.size(); i++) { //for all objects in the new frame
            rectangle(outMat, objRectangles[i].tl(), objRectangles[i].br(), rectColor, 2, 8, 0); //draw rectangle on image
        }
    }

    prevScore.clear(); //clear vector
    prevScore.swap(actuScore); //copy actualscore to prevscore
    actuScore.clear();

}


void actors_classification_v3(Mat detectionMask, Mat &houghFrame, GuiParam param, int paused, vector<Rect> & objRectangles, vector<int>& prevScore, vector<int>& actuScore) {//GuiParam param, int paused, Mat detectionMask, Mat& houghFrame, Mat& outMat, /*vector<vector<Point> >& actorsContour,*/ vector<Rect> & objRectangles, vector<Rect>& prevRectangles, vector<Rect>& actuRectangles, vector<int>& prevScore, vector<int>& actuScore) {
    //Number of objects
    int nbObj = objRectangles.size();

    //Compute y position
    Point newObjCentroid;
    for (size_t i = 0; i < nbObj; i++) { //for all objects in the new frame
        newObjCentroid = getRectCentroid(objRectangles[i]);
    }

    //compute line ratio
    double lineMin = param.hough_line_min + (newObjCentroid.y / LENGTH_CORRECTION);

    Mat actorsBW; //convert to black&white
    cvtColor(detectionMask, actorsBW, CV_BGR2GRAY);

    //Canny detector
    Mat cannyMat;
    cv::Canny(actorsBW, cannyMat, param.canny_t1, param.canny_t2, 3, false);

    //Mat houghFrame;
    cvtColor(cannyMat, houghFrame, COLOR_GRAY2BGR); //
    vector<Vec4i> lines;

    //Hough lines detector then draw the lines
    HoughLinesP(cannyMat, lines, 1, CV_PI / 180, param.hough_accu_tresh, lineMin/*param.hough_line_min*/, param.hough_max_gap); //1, CV_PI / 180, 10, 1, 1);
    for (size_t i = 0; i < lines.size(); i++) {
        line(houghFrame, Point(lines[i][0], lines[i][1]),
                Point(lines[i][2], lines[i][3]), Scalar(0, 255, 0), LINE_THICKNESS + 1, 8);
    }

    //Compute score
    if (nbObj > 0) {
        int score;
        if (paused)
            score = prevScore[0];
        else
            score = compute_score(lines.size(), prevScore[0]);
        //cout << "nbLines: " << lines.size() << " score: " << score << endl;
        actuScore.push_back(score);
    }

    prevScore.clear(); //clear vector
    prevScore.swap(actuScore); //copy actualscore to prevscore
    actuScore.clear();
}

void annoteFrame(Mat frame, Mat& annotedFrame, vector<Rect> & objRectangles, vector<int>& prevScore) {
  //Draw bounding rectangles
    if (prevScore.size() > 0) {
        Scalar rectColor;
        if (prevScore[0] > SCORE_THRESHOLD_MACHINE) {
            //cout << "chariot" << endl;
            rectColor = MACHINE_COLOR;
        } else if (prevScore[0] < SCORE_THRESHOLD_HUMAN) {
            //cout << "pieton" << endl;
            rectColor = HUMAN_COLOR;
        } else {
            //cout << "unknown" << endl;
            rectColor = UNKNOWN_COLOR;
        }
        annotedFrame = frame.clone();
        for (size_t i = 0; i < objRectangles.size(); i++) { //for all objects in the new frame
            rectangle(annotedFrame, objRectangles[i].tl(), objRectangles[i].br(), rectColor, 2, 8, 0); //draw rectangle on image
        }
    }
}