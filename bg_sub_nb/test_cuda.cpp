#include "obj_detect.h"
#include "Log.h"
#include <pthread.h>
#include <thread>

#include "cudabgsegm.hpp" //cuda bgsub

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/cudabgsegm.hpp"
//#include "opencv2/cudalegacy.hpp"
#include "opencv2/video.hpp"
//#include "opencv2/highgui.hpp"
//#include "opencv2/core/cuda.hpp"

#include <opencv2/core/opengl.hpp>
#include <opencv2/cudacodec.hpp>
#include <opencv2/highgui.hpp>


using namespace std;
using namespace cv;
using namespace cv::cuda;

void cuda_wo_stream() {
    cout << "cuda bg sub test: start" << endl;
    char* path = "/home/sylvain/Videos/jpn/japan4K_div6_q21.mp4";
    VideoCapture cap = openVid(path);
    int key = 0;
    Mat frame;
    cap >> frame;
    int cpt = 0;
    GpuMat d_frame(frame);

    Ptr<BackgroundSubtractor> mog2 = cuda::createBackgroundSubtractorMOG2();

    GpuMat d_fgmask;
    GpuMat d_fgimg;
    GpuMat d_bgimg;

    Mat fgmask;
    Mat fgimg;
    Mat bgimg;

    mog2->apply(d_frame, d_fgmask);
    namedWindow("image", WINDOW_NORMAL);
    namedWindow("foreground mask", WINDOW_NORMAL);
    namedWindow("foreground image", WINDOW_NORMAL);
    for (;;) {

        cap >> frame;
        if (frame.empty())
            break;
        double durationUpload = (double) getTickCount();
        d_frame.upload(frame);
        durationUpload = (double) getTickCount() - durationUpload;
        //int64 start = cv::getTickCount();

        double durationMog = (double) getTickCount();
        mog2->apply(d_frame, d_fgmask);
        durationMog = (double) getTickCount() - durationMog;

        mog2->getBackgroundImage(d_bgimg);

        //double fps = cv::getTickFrequency() / (cv::getTickCount() - start);
        //  std::cout << "FPS : " << fps << std::endl;

        d_fgimg.create(d_frame.size(), d_frame.type());
        d_fgimg.setTo(Scalar::all(0));
        d_frame.copyTo(d_fgimg, d_fgmask);

        double durationDownload = (double) getTickCount();
        d_fgmask.download(fgmask);
        durationDownload = (double) getTickCount() - durationDownload;
        d_fgimg.download(fgimg);
        if (!d_bgimg.empty())
            d_bgimg.download(bgimg);

        imshow("image", frame);
        imshow("foreground mask", fgmask);
        imshow("foreground image", fgimg);
        if (!bgimg.empty())
            imshow("mean background image", bgimg);


        cout << "upload : " << durationUpload / getTickFrequency() *1000 << " mog : " << durationMog / getTickFrequency() *1000 << " download: " << durationDownload / getTickFrequency() *1000 << endl;

        if (cpt++ == 1500)
            break;


        key = waitKey(30);
        if ((char) key == 27)
            break;
    }
    cout << "cuda bg sub test: end" << endl;

}

/**
 * Test cuda function with stream. Because without stream the initialisation (frame.upload) phase
 * takes a lot of time. 
 */
void cuda_w_stream() {
    const string fname = "/home/sylvain/Videos/jpn/japan4K_div1_q21.mp4";
    int key = 0;
    int cpt = 0;
    cv::namedWindow("CPU", cv::WINDOW_NORMAL);
    cv::namedWindow("GPU", cv::WINDOW_OPENGL);
    cv::cuda::setGlDevice();
   
    cv::Mat frame;
    cv::VideoCapture reader(fname);

    cv::cuda::GpuMat d_frame;
    cv::Ptr<cv::cudacodec::VideoReader> d_reader = cv::cudacodec::createVideoReader(fname);

    double durationPrev = (double)getTickCount();
    
    for (;;) {
         double durationActu = (double) getTickCount();
         
           cout << "upload : " << (durationActu-durationPrev) / getTickFrequency() *1000 <<endl;
           durationPrev = durationActu;
         
        if (!reader.read(frame))
            break;

        if (!d_reader->nextFrame(d_frame))
            break;

        
        
        
        cv::imshow("CPU", frame);
        cv::imshow("GPU", d_frame);

        key = waitKey(1);
        if ((char) key == 27)
            break;

        if (cpt++ == 200)
            break;

    }
    cout << "endl" << endl;

}

void cuda_bgsub() {
    //   cuda_wo_stream();
    cuda_w_stream();
}