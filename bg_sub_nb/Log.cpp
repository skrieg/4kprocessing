/*
 * Log.cpp
 *
 *  Created on: Aug 9, 2015
 *  Author: jonathan
 */

#include "Log.h"

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>


using namespace std;

//namespace Core {

Log::e_log_type Log::type = LOG;
string Log::directory = string("");

Log::Log() {
}

Log::~Log() {
}

/**
 * Clear file content
 * @param dir
 */
void Log::clearFile(std::string dir) {
    std::ofstream ofs;
    ofs.open(dir.c_str(), std::ofstream::out | std::ofstream::trunc);
    if (!ofs.is_open()) {
        perror("Error while opening log file, function clearFile");   
    }
    ofs.close();
}

/**
 * Write line
 * @param text
 * @param dir
 */
void Log::write(std::string text, std::string dir) {
    ofstream fp;
    fp.open(dir.c_str(), std::ofstream::out | std::ofstream::app);
    if (!fp.is_open()) {
        perror("Error while opening log file, function write");
    }
    fp << text << endl;
    fp.close();
}

/**
 * Set the directory and filne name
 * @param dir
 */
void Log::setDirectory(string dir) {
    char temp[255];
    getcwd(temp, 255);
    if (dir.find('/') == 0)
        dir = dir.substr(1, dir.length());
    //directory = string(temp, strlen(temp)) + dir;
    //dir.copy((char*)directory.c_str(), dir.length(), 0);
    //strcpy((char*)directory.c_str(), (char*)dir.c_str());
    //dir.copy(directory, dir.length(), 0 );
    //cout << "directory set:"<<directory<<" bordel!! "<<dir<<"len= "<<dir.length()<<endl;
}

/**
 * 
 * @param level
 * @param text
 * @param dir
 */
void Log::write(e_log_type level, std::string text, std::string dir) {
#if defined (DEBUG)
    cout << getTimestamp() + " [" + getLevel(level) + "] " + text << endl;
#endif
    if (type <= level) {
        ofstream fp;
        //fp.open ("/home/sylvain/tm/bg_sub_nb/log/sequencer.txt", std::ofstream::out | std::ofstream::app);
        fp.open(dir.c_str(), std::ofstream::out | std::ofstream::app);

        if (!fp.is_open()) {
            perror("Error while opening log file");
        }
        fp << getTimestamp() + " [" + getLevel(level) + "] ";
        fp << text << endl;
        fp.close();
    }
}

void Log::setLevel(e_log_type level) {
    type = level;
}

string Log::getLastError() {
    char * error = strerror(errno);
    return ": " + string(error, strlen(error));
}

//void	Log::write (string prefix, int written, int expected, uint8_t * data)
//{
//	stringstream ss;
//	ss << written;
//	string m (prefix + " " + ss.str()+" of ");
//	ss.str("");
//	ss << expected;
//	m += ss.str() + " Bytes written with message: " + string ((const char *)data, expected - 2);
//	Log::write (VERBOSE, m);
//}

//
// Private method
//

string Log::getTimestamp() {
    char * date;
    time_t now = time(0);
    date = asctime(gmtime(&now));
    string timestamp(date, strlen(date));
    return timestamp.substr(0, timestamp.length() - 1);
}

string Log::getLevel(e_log_type level) {
    switch (level) {
        case VERBOSE: return string("VERBOSE");
        case INFO: return string("INFO");
        case LOG: return string("LOG");
        case ERROR: return string("ERROR");
        case CRITICAL: return string("CRITICAL");
    }
    return string("unknown log");
}

//} /* namespace Sensiwall */
