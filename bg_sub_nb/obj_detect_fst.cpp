/**
 * @brief Fast object detection
 * @file obj_detect_fst.cpp
 * @author sylvainkrieg@gmail.com
 * @date June 2016
 */

/*
 * convert to low res
 * mog2
 * segmntation
 * detection
 * high res classification
 * 
 * Select ROI
 */

#include "obj_detect.h"

namespace fst {
    int paused;
    GuiParam param;
    int frameNumber = 0;
    bool firstImage = true;
    stringstream logStream; //log

    /**
     * Convert tickCount to time duration.
     * @param tickCount
     * @return 
     */
    double compute_duration(double tickCount) {
        return tickCount / getTickFrequency() *1000;
    }
}



using namespace cv;
using namespace std;
using namespace fst;

void ffps(const char* videoFilename, const char* logPath) {
    VideoCapture capture;
    string tmp = string(videoFilename);
    cout << "-------------Running fst (mono-task) : " << videoFilename << endl;
    Mat frame; //frame from file
    //Mat preprocFrame; //frame after preprocessing
    Mat fgFrame; //foreground frame (after background subtraction)
    Mat filteredFrame;
    Mat segmntFrame; //frame after segmentation
    Mat detectionFrame; //frame with highlighted detected objects
    Mat actualFrameWin1; //selected frame for display (window 1)
    Mat actualFrameWin2; //selected frame for display (window 2))

    Mat resizedFrame1; //resized frame for display 
    Mat resizedFrame2; //resized frame for display
    Mat structuringElement;
    
    Mat lowResFrame;

    //Mat resultFrame;
    Mat fgGrid; //grid for operation gridfill

    int keyboard = 0; //input from keyboard
    double FpsSum = 0; //to count fps
    fst::paused = false; //global pause 

    Ptr<BackgroundSubtractorMOG2> pSub;
    Ptr<BackgroundSubtractorKNN> pSubKNN;

    int srcWin1Trkbar = 1;
    int srcWin2Trkbar = 5;
    String txtSrc1, txtSrc2;

    //vector<KeyPoint> keyImg; //contain keypoints from blob detection

    String windowName1 = "visual1";
    String windowName2 = "visual2";
    namedWindow(windowName1, WINDOW_AUTOSIZE);
    namedWindow(windowName2, WINDOW_AUTOSIZE);
    cvCreateTrackbar("source", "visual1", &srcWin1Trkbar, 5, NULL);
    cvCreateTrackbar("source", "visual2", &srcWin2Trkbar, 5, NULL);

    createGUI(windowName1, &param);
    cout << "tmpstring::" << tmp << endl;
    capture = openVid(tmp); //"/home/skrieg/vid/cam02/chariots/20130723_001452_s01c02.avi");
    //capture = openVid(videoFilename);
    printVidInfo(capture);

    pSub = createBackgroundSubtractorMOG2(); //MOG2 approach
    pSubKNN = createBackgroundSubtractorKNN();

    //Config
    blob_detector_config(&param);

    std::vector<Rect> prevRectangles;
    std::vector<Rect> actuRectangles;
    std::vector<int> prevScore;
    std::vector<int> actualScore;
    int pixFound;

    //log file
    Log::clearFile(logPath);
    logStream << logPath << endl;
    logStream << "nb_lines surface ratio" << endl;
    Log::write(logStream.str(), logPath);
    logStream.str(std::string()); //clear stream

    //Video writer
    String test = String(videoFilename) + "out.avi";
    char* outputName = (char*) test.c_str(); //"out.avi";
    //VideoWriter video(outputName, CV_FOURCC('P','I','M','1'), 30, Size(640, 480), true);
    //VideoWriter video(outputName, CV_FOURCC('P','I','M','1'), 30, /*Size(640, 480)*/Size(3840,2160), true);
    cout << "save video as:" << outputName << endl;
    //    VideoWriter outputVideo; // Open the output
    //    if (askOutputType)
    //        outputVideo.open(NAME, ex = -1, inputVideo.get(CAP_PROP_FPS), S, true);
    //    else
    //        outputVideo.open(NAME, ex, inputVideo.get(CAP_PROP_FPS), S, true);
    //
    //    if (!outputVideo.isOpened()) {
    //        cout << "Could not open the output video for write: " << source << endl;
    //        return -1;
    //    }

    while ((char) keyboard != 'q' && (char) keyboard != 27) {//read input data. ESC or 'q' for quitting
        double ticksStart = (double) getTickCount();
        double durationTotal = (double) getTickCount();

        //Pause with space bar 
        if ((char) keyboard == ' ') {
            paused = !paused;
        }

        if (!paused) {
            //read the current frame
            if (!capture.read(frame)) {
                cerr << "Unable to read next frame." << endl;
                cerr << "Exiting..." << endl;
                exit(EXIT_FAILURE);
            }
            frameNumber++;
        }

        ///////////////////////////////////////////////////////
        //Convert to low res
        ///////////////////////////////////////////////////////
       // resize(frame, lowResFrame, LOW_RES_SIZE, 0, 0, INTER_NEAREST);

        ///////////////////////////////////////////////////////
        //Background subtraction
        ///////////////////////////////////////////////////////
        backGroundSubMog2(param, frame, fgFrame, pSub, paused); //bg sub
        cv::threshold(fgFrame, fgFrame, 254, 255, cv::THRESH_BINARY); //remove shadow 

        ///////////////////////////////////////////////////////
        // Segmentation
        //Noise filtering Open / close
        ///////////////////////////////////////////////////////

        filteredFrame = fgFrame.clone();

        if (param.openActive) { //apply opening and closing operations
            structuringElement = getStructuringElement(MORPH_RECT, Size(2 * param.opnElmt_trkbar + 1, 2 * param.opnElmt_trkbar + 1), Point(-1, -1)); //set structuring element
            morphologyEx(filteredFrame, filteredFrame, MORPH_OPEN, structuringElement); //opening 
        }
        if (param.closeActive) {
            structuringElement = getStructuringElement(MORPH_RECT, Size(2 * param.closElmt_trkbar + 1, 2 * param.closElmt_trkbar + 1), Point(-1, -1)); //set structuring element
            morphologyEx(filteredFrame, filteredFrame, MORPH_CLOSE, structuringElement); //closing
        }
        if (param.DilateActive) { // Apply the erosion operation
            structuringElement = getStructuringElement(MORPH_RECT, Size(param.dilateElmt_trkbar + 1, 2 * param.dilateElmt_trkbar + 1),
                    //Point(_param.eroElmt_trkbar, _param.eroElmt_trkbar));
                    Point(-1, -1));
            //erode(filteredFrame, filteredFrame, structuringElement);
            dilate(filteredFrame, filteredFrame, structuringElement);
        }

        if (param.gridFillActive) {
            const int boxSizeX = 4;
            const int boxSizeY = 4;
            int gridW = filteredFrame.cols / boxSizeX; //320;256
            int gridH = filteredFrame.rows / boxSizeY; //180;144
            if (!check_grid_dividers(filteredFrame, gridW, gridH))
                exit(EXIT_FAILURE);
            fgGrid = Mat::zeros(gridH, gridW, CV_8U);
            pixFound = fill_grid(filteredFrame, fgGrid);
            fill_img_with_grid(fgGrid, filteredFrame);
            if (param.holesFillActive) {
                grid_fill_holes(filteredFrame, fgGrid, 4, 255);
            }
            segmntFrame = filteredFrame.clone();
        } else {
            //use_floodfill(filteredFrame, segmntFrame);
            segmntFrame = filteredFrame.clone();
        }


        ///////////////////////////////////////////////////////
        //Detection
        ///////////////////////////////////////////////////////

        Mat detectionMask = Mat::zeros(fgFrame.size(), fgFrame.type());
        Mat detectionColorMask = Mat::zeros(frame.size(), frame.type());
        vector<vector<Point> > actorsContour;
        vector<Rect> actorsRectangle;
        detect_contours(param, segmntFrame, frame, detectionColorMask, actorsContour, actorsRectangle);

        ///////////////////////////////////////////////////////
        //Classification
        /////////////////////////////////////////////////////// 

        Mat annotedFrame = Mat::zeros(filteredFrame.size(), filteredFrame.type());
        Mat houghFrame = Mat::zeros(filteredFrame.size(), filteredFrame.type());
        //actors_classification(param, paused, frame, detectionColorMask, annotedFrame, actorsContour, actorsRectangle, prevRectangles, actuRectangles, prevScore, actualScore);
        //v2
        //actors_classification_v2(param, paused, detectionColorMask, houghFrame, annotedFrame, /*actorsContour,*/ actorsRectangle, prevRectangles, actuRectangles, prevScore, actualScore);
//        actors_classification_v3(detectionColorMask, houghFrame, param, paused, actorsRectangle, prevScore, actualScore);
//        annoteFrame(frame, annotedFrame, actorsRectangle, prevScore);

        //Write the score in the log file
        /*if (prevScore.size() > 0) {
            logStream << prevScore[0];
        }*/
        prevScore.push_back(0);
        /*if (logStream.str().length() > 0)
            Log::write(logStream.str(), logPath);
        logStream.str(std::string()); //clear stream
         */
        //end v2

        //Clear memory
        ///////////////////////////////////////////////////////
        actorsContour.clear();
        actorsRectangle.clear();

        ///////////////////////////////////////////////////////
        //Visualisation
        ///////////////////////////////////////////////////////
        double durationResize = (double) getTickCount();
        //window1 source selection
        switch (srcWin1Trkbar) {
            case SRC_RAW:
                actualFrameWin1 = frame.clone();
                txtSrc1 = "RAW";
                break;
            case SRC_PREPROC:
                actualFrameWin1 = fgFrame.clone();
                txtSrc1 = "ForeGround";
                break;
            case SRC_SEGMNT:
                actualFrameWin1 = segmntFrame.clone();
                txtSrc1 = "SEGMENTATION";
                break;
            case SRC_DETECT:
                actualFrameWin1 = detectionColorMask.clone();
                txtSrc1 = "DETECTION";
                break;
            case SRC_HOUGH:
                actualFrameWin1 = houghFrame.clone();
                txtSrc1 = "HOUGH";
                break;
            case SRC_RESULT:
                actualFrameWin1 = annotedFrame.clone();
                txtSrc1 = "FINAL";
                break;
            default:
                cout << "error: source selection unknown" << srcWin1Trkbar << endl;
        }

        //resize(actualFrameWin1, actualFrameWin1, Size(960, 540), 0, 0, INTER_LINEAR);
        resize(actualFrameWin1, resizedFrame1, RESIZE_SIZE, 0, 0, INTER_NEAREST);

        putText(resizedFrame1, txtSrc1.c_str(), cv::Point(15, 40), FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));
        //FPS Counter
        stringstream ss;
        rectangle(resizedFrame1, cv::Point(10, 2), cv::Point(100, 20), cv::Scalar(255, 255, 255), -1);
        ss << ((int) (FpsSum / AVG_FPS_SIZE));
        string frameNumberString = ss.str();
        putText(resizedFrame1, frameNumberString.c_str(), cv::Point(15, 15), FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));



        //window2 source selection
        switch (srcWin2Trkbar) {
            case SRC_RAW:
                actualFrameWin2 = frame.clone();
                txtSrc2 = "RAW";
                break;
            case SRC_PREPROC:
                actualFrameWin2 = fgFrame.clone();
                txtSrc2 = "ForeGround";
                break;
            case SRC_SEGMNT:
                actualFrameWin2 = segmntFrame.clone();
                txtSrc2 = "SEGMENTATION";
                break;
            case SRC_DETECT:
                actualFrameWin2 = detectionColorMask.clone();
                txtSrc2 = "DETECTION";
                break;
            case SRC_HOUGH:
                actualFrameWin2 = houghFrame.clone();
                txtSrc2 = "HOUGH";
                break;
            case SRC_RESULT:
                actualFrameWin2 = annotedFrame.clone();
                txtSrc2 = "FINAL";
                break;
            default:
                cout << "error: source selection unknown" << srcWin2Trkbar << endl;
        }
        resize(actualFrameWin2, resizedFrame2, RESIZE_SIZE, 0, 0, INTER_NEAREST);
        durationResize = (double) getTickCount() - durationResize;
        double durationShow = (double) getTickCount();
        putText(resizedFrame2, txtSrc2.c_str(), cv::Point(15, 40), FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255));

        imshow(windowName1, resizedFrame1);
        //imshow(windowName1, actualFrameWin1);
        durationShow = (double) getTickCount() - durationShow;

        imshow(windowName2, resizedFrame2);

        durationTotal = (double) getTickCount() - durationTotal;

        keyboard = waitKey(1); //get the input from the keyboard

        if (FpsSum == 0) { //first value
            FpsSum = AVG_FPS_SIZE * (1 / (((double) getTickCount() - ticksStart) / getTickFrequency())); //function elapsed time=(actualTick-tickStart)/tickFreq
        } else {
            FpsSum = (FpsSum - FpsSum / AVG_FPS_SIZE)+(1 / (((double) getTickCount() - ticksStart) / getTickFrequency()));
        }

        //write video output
        //video.write(annotedFrame);
        stringstream logStream;
        using namespace fst;
        logStream << "frame: " << frameNumber;
        logStream << " " << compute_duration(durationTotal);

        //Log::setLevel(Log::VERBOSE);
        //Log::write(Log::INFO, logStream.str(), logPath);

        Log::write(logStream.str(), logPath);
        logStream.str(std::string()); //clear stream

    }
    //delete capture object
    capture.release();

}