#include "obj_detect.h"

void printVidInfo(VideoCapture capt) {

    cout << "-------Video info-------" << endl;
    cout << "-- Width:" << capt.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
    cout << "-- Height:" << capt.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;
}
