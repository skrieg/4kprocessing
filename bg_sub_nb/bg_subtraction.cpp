/**
 * @brief Backround subtraction functions
 * @file bg_subtraction.cpp
 * @author sylvain.krieg@master.hes-so.ch
 * @date decembre 2015
 */


#include "obj_detect.h"

/**
 * Background subtraction using KNN method
 * @param param parameters list
 * @param frame input frame
 * @param retFrame output frame
 * @param pSub subtractor model
 * @param pause stop learning update
 * @return background frame
 */
Mat backGroundSubKNN(GuiParam param, cv::Mat &srcFrame, cv::Mat &dstFrame, BackgroundSubtractorKNN* pSub, int pause) {
    pSub->setDetectShadows(param.ShadowActive);
    pSub->setDetectShadows(true);
    pSub->setDist2Threshold((double) param.KnnDist2Threshold);
    pSub->setNSamples((double) (2));
    pSub->setShadowValue((double) param.Mog2Complexity_trkbar / 10);
    pSub->setkNNSamples((double) param.kNNSamples);
    pSub->setShadowThreshold((double) param.shadow_trkbar / 100); //set shadow threshold (default 0.5)

    pSub->setHistory(param.history_trkbar + 50); //set mog2 history (default 500)
    pSub->apply(srcFrame, dstFrame, pause ? 0 : -1); //0 = no update, -1 = automatic learning rate
    return dstFrame;
}

/**
 * Background subtraction using MOG2 method
 * @param param parameters list
 * @param frame input frame
 * @param retFrame output frame 
 * @param pSub subtractor model
 * @param pause stop learning update
 * @return background frame
 */
Mat backGroundSubMog2(GuiParam param, cv::Mat &srcFrame, cv::Mat &dstFrame, BackgroundSubtractorMOG2* pSub, int pause) {
    pSub->setDetectShadows(param.ShadowActive);
    pSub->setBackgroundRatio((double) (param.Mog2Ratio_trkbar + 1) / 100); //set background ration (default 0.9)
    pSub->setComplexityReductionThreshold((double) param.Mog2Complexity_trkbar / 10);
    pSub->setNMixtures(param.mixture_trkbar); //set NMixture (default 5), do not use
    pSub->setShadowThreshold((double) param.shadow_trkbar / 100); //set shadow threshold (default 0.5)
    //pSub->setVarInit((double)param.mixture_trkbar);
    pSub->setVarThreshold((double) param.Mog2Variance_trkbar + 1); //set variance threshold (default 15) [>100]
    //        pSub->setVarThresholdGen((double)param.mixture_trkbar*10); //has no effect
    pSub->setHistory(param.history_trkbar + 50); //set mog2 history (default 500)
    pSub->apply(srcFrame, dstFrame, pause ? 0 : -1); //0 = no update, -1 = automatic learning rate
    return dstFrame;
}
