/**
 * Fill holes in a black-white image using a grid. When a pixel is '1', all the
 * pixels in the corresponding grid are set to '1'.
 * 
 * 1. check if the dividers are suitable for the image using check_grid_dividers
 * 2. compute the grid using grid_fill
 * 3. use fill_img_with_grid to get the resulting image
 * 
 * @param imgIN
 * @name Sylvain Krieg
 * @date 4 nov. 2015
 */

#include "obj_detect.h"

/**
 * Print the possible dividers
 * @param imgIN
 */
void get_possible_dividers(Mat& imgIN) {
    bool smallerThanImg = true;
    int divider = 2;
    cout << "img can be divider by ";
    while (smallerThanImg) {
        if (imgIN.rows % divider == 0 && imgIN.cols % divider == 0) {
            cout << divider << " ";
        }
        divider = divider + 2;
        if (divider >= imgIN.rows || divider >= imgIN.cols) {
            cout << endl;
            smallerThanImg = false;
        }
    }
}

/**
 * Check if the dividers can fit with the image size.
 * @param imgIN
 * @param divWidth
 * @param divHeight
 * @return true when ok, false otherwise
 */
bool check_grid_dividers(Mat& imgIN, int divWidth, int divHeight) {
    //Image width needs to be a multiple of the widht divider, same for height
    if (imgIN.cols % divWidth != 0 || imgIN.rows % divHeight != 0) {
        cout << "Error: parameters for grid divider can't fit with the image" << endl;
        cout << "col=" << imgIN.cols << " divWidth=" << divWidth << endl;
        cout << "rows=" << imgIN.rows << " divHeight=" << divHeight << endl;
        //cout << " mod width: " << imgIN.cols % divWidth << " height: " << imgIN.rows % divHeight << endl;
        return false;
    }
    return true;
}

/**
 * Convert image coord(1 axe) to grid index
 * @param size
 * @param coord
 * @param div
 * @return 
 */
int coord_to_grid(int size, int coord, int div) {
    int step = size / div;
    return coord / step;
}

/**
 * Analyze the input image and fill the grid. 
 * @param imgIN black&white image
 * @param gridINOUT
 * @param divX
 * @param divY
 */
int fill_grid(Mat& imgIN, Mat& gridINOUT) {
    int imgBoxSizeX = imgIN.cols / gridINOUT.cols;
    int imgBoxSizeY = imgIN.rows / gridINOUT.rows;
    int imgBoxStartX, imgBoxStartY, imgBoxEndX, imgBoxEndY = 0;
    //int xIndex = 0, yIndex = 0;
    int pixFound = 0;
    bool boxDone = false;
    //parse the image grid box by grid box
    for (int gridIndexX = 0; gridIndexX < gridINOUT.cols; gridIndexX++) {
        for (int gridIndexY = 0; gridIndexY < gridINOUT.rows; gridIndexY++) {
            //set the pixel index at the beginning of the box
            //xIndex = gridIndexX * imgBoxSizeX;
            //yIndex = gridIndexY * imgBoxSizeY;
            //initialize starting positions of the box
            imgBoxStartX = gridIndexX*imgBoxSizeX;
            imgBoxStartY = gridIndexY*imgBoxSizeY;
            imgBoxEndX = imgBoxStartX + imgBoxSizeX;
            imgBoxEndY = imgBoxStartY + imgBoxSizeY;
            boxDone = false;
            //parse all the pixels of the full scale image corresponding to the box x,y
            for (int xIndex = imgBoxStartX; (xIndex < imgBoxEndX) && !boxDone; xIndex++) {
                for (int yIndex = imgBoxStartY; (yIndex < imgBoxEndY) && !boxDone; yIndex++) {
                    if (imgIN.at<uchar>(yIndex, xIndex) == 255) {
                        //gridINOUT.at<uchar>(coord_to_grid(imgIN.rows, y, divY), coord_to_grid(imgIN.cols, x, divX)) = 255;
                        gridINOUT.at<uchar>(gridIndexY, gridIndexX) = 255;
                        boxDone = true;
                        pixFound++;
                    }
                }
            }

        }
    }
    return pixFound;
}

void _grid_fill(Mat& imgIN, Mat& gridINOUT, int divX, int divY) {

    //parse all the pixels of the image
    for (int x = 0; x < imgIN.cols; x++) {
        for (int y = 0; y < imgIN.rows; y++) {
            if (imgIN.at<uchar>(y, x) == 255) {
                gridINOUT.at<uchar>(coord_to_grid(imgIN.rows, y, divY), coord_to_grid(imgIN.cols, x, divX)) = 255;
            }
        }
    }
}

/**
 * Fill the full scale image from the grid
 * @param gridIN
 * @param imgOUT
 */
void fill_img_with_grid(Mat& gridIN, Mat& imgOUT) {
    //parse the gridd, when a point is white, fill the corresponding pixels in the output img
    int imgBoxSizeX = imgOUT.cols / gridIN.cols;
    int imgBoxSizeY = imgOUT.rows / gridIN.rows;
    int imgBoxStartX, imgBoxStartY, imgBoxEndX, imgBoxEndY = 0;
    //parse the grid
    for (int gridIndexX = 0; gridIndexX < gridIN.cols; gridIndexX++) {
        for (int gridIndexY = 0; gridIndexY < gridIN.rows; gridIndexY++) {
            if (gridIN.at<uchar>(gridIndexY, gridIndexX) == 255) {//position in the grid in '1'
                //cout << " pos "<<gridIndexX<<" "<<gridIndexY<<endl;

                //initialize starting positions of the box
                imgBoxStartX = gridIndexX*imgBoxSizeX;
                imgBoxStartY = gridIndexY*imgBoxSizeY;
                imgBoxEndX = imgBoxStartX + imgBoxSizeX;
                imgBoxEndY = imgBoxStartY + imgBoxSizeY;

                //fill the box corresponding in the full scale image
                for (int xIndex = imgBoxStartX; xIndex < imgBoxEndX; xIndex++) {
                    for (int yIndex = imgBoxStartY; yIndex < imgBoxEndY; yIndex++) {
                        imgOUT.at<uchar>(yIndex, xIndex) = 255;
                    }
                }
            }
        }
    }
}

void fill_holes() {

}

/**
 * convert grid position to image position
 * @param sizeImg
 * @param coord
 * @param div
 * @return 
 */
int grid_to_coord(int sizeImg, int coord, int div) {
    return coord / div;
}

/**
 * Evaluate a case value (by weighting neighbors)
 * @param gridX IN
 * @param gridY IN
 * @return compute value, or 0 if error
 */
int grid_evaluate_case_neighbors(int gridX, int gridY, Mat fgGrid_) {
    int value = 0;

    // Don't evaluate borders
    if ((gridX == 0) || (gridY == 0))
        return (value);

    if ((gridX > fgGrid_.cols - 2) || (gridY > fgGrid_.rows - 2))
        return (value);

    /*
     * Check Left side
     */
    {
        int x = gridX - 1;

        // Top Left cell
        if (gridY > 0)
            if (fgGrid_.at<uchar>(gridY - 1, x)) value++;

        // Middle Left cell
        if (fgGrid_.at<uchar>(gridY, x)) value++;

        // Bottom Left cell
        if (gridY < fgGrid_.rows)
            if (fgGrid_.at<uchar>(gridY + 1, x)) value++;
    }

    /*
     * Check Center
     */
    // Top Center cell
    if (gridY > 0)
        if (fgGrid_.at<uchar>(gridY - 1, gridX)) value++;

    // Bottom Center cell
    if (gridY < fgGrid_.rows)
        if (fgGrid_.at<uchar>(gridY + 1, gridX)) value++;

    /*
     * Check Right side
     */
    {
        int x = gridX + 1;

        // Top Right cell
        if (gridY > 0)
            if (fgGrid_.at<uchar>(gridY - 1, x)) value++;

        // Middle Right cell
        if (fgGrid_.at<uchar>(gridY, x)) value++;

        // Bottom Right cell
        if (gridY < fgGrid_.rows)
            if (fgGrid_.at<uchar>(gridY + 1, x)) value++;
    }

    return (value);
} // grid_evaluate_case_neighbors



/****************************************************************************/

/**
 * Fill a cases 
 * @param coordX OUT coord of the next case
 * @param coordY OUT
 * @param color IN 0=black..255=white
 * @return true when done
 */
bool fill_a_case(Mat& fgClean_, Mat& fgGrid_, int coordX, int coordY, int divX, int divY, int color) {
    int stepX = fgClean_.cols / divX;
    int stepY = fgClean_.rows / divY;

    int topX = (coordX / stepX) * stepX;
    int topY = (coordY / stepY) * stepY;
    int botX = topX + stepX - 1;
    int botY = topY + stepY - 1;

    // Fill case
    for (int y = topY; y <= botY; y++) {
        for (int x = topX; x <= botX; x++)
            fgClean_.at<uchar>(y, x) = color;
    }

    // Update grid
    {
        int gridX = 0;
        int gridY = 0;
        //coord_to_grid(fgClean_, coordX, coordY, gridX, gridY);
        gridX = grid_to_coord(fgClean_.cols, coordX, divX);
        gridY = grid_to_coord(fgClean_.rows, coordY, divY);
        fgGrid_.at<uchar>(gridY, gridX) = 255;
    }

    // Jump to next case (on the left)
    if (coordX < fgClean_.cols - stepX)
        coordX = botX;

    return (true);
} // fill_a_case

/**
 * Fill holes in blob shape
 * @param threshold IN default=4
 * @param gridY IN default=255
 * @return number of cell filled
 */
int grid_fill_holes(Mat& fgClean_, Mat& fgGrid_, int threshold, int color) {

    int value = 0;
    //int coordX = 0;
    //int coordY = 0;
    int cellModified = 0;
    int divX = fgClean_.cols / fgGrid_.cols;
    int divY = fgClean_.rows / fgGrid_.rows;

    // Scan grid
    int rows = fgGrid_.rows - 1;
    int cols = fgGrid_.cols - 1;
    //cout << "func"<<endl;
    for (int y = 1; y < rows; ++y) {
        //cout << "row:" << y <<endl;        
        for (int x = 1; x < cols; ++x) {

            // Check only black cell
            if (fgGrid_.at<uchar>(y, x) == 0) {
                value = grid_evaluate_case_neighbors(x, y, fgGrid_);

                if (value >= threshold) {
                    // cout<<" val:"<<value;
                    // int coordX = grid_to_coord(fgClean_.cols, x, divX);
                    // int coordY = grid_to_coord(fgClean_.rows, y, divY);
                    fgGrid_.at<uchar>(y, x) = 255;
                    //grid_to_coord(fgClean_, x, y, coordX, coordY);
                    //fill_a_case(Mat& fgClean_, int& coordX, int& coordY, int divX, int divY, int color)
                    //fill_a_case(fgClean_, fgGrid_, coordX, coordY, divX, divY, color);
                    //fill_img_with_grid(fgClean_, fgGrid_);
                    cellModified++;
                    /*
                     * WARNING:
                     * if a cell is modify start line scan again
                     */
                    //    x -= 2;
                    //    y--; // Produce wrong shape (fill too much) 

                }
            }
        }
        fill_img_with_grid(fgGrid_, fgClean_);
    }

    //	cout << "cell filled: " << cellModified << endl;

    return (cellModified);
} // grid_fill_holes

int filling_holes_test() {
    ///home/sylvain/Dropbox/master/TM/7_import/img_bank/coins.png
    //string imgPath = "/home/sylvain/Dropbox/master/TM/7_import/img_bank/coins.png";
    //string imgPath = "/home/sylvain/Dropbox/master/TM/7_import/img_bank/imgtest.png";
    string imgPath = "/home/sylvain/Dropbox/master/TM/7_import/img_bank/voit.png";
    int gridWidth = 100; //50 12;
    int gridHeight = 75; //41 6;
    //string imgPath = "/home/skrieg/Current/master/tm/7_import/img_bank/coins.png";
    //string imgPath = "/home/skrieg/travail/Current/master/tm/7_import/img_bank/imgtest.png";
    //string imgPath = "/home/skrieg/travail/Current/master/tm/7_import/img_bank/coins.png";
    //string imgPath = "/home/skrieg/travail/Current/master/tm/7_import/img_bank/voit.png";



    //    double duration = (double) getTickCount();

    Mat img = imread(imgPath.c_str(), 0);

    if (img.empty()) {
        cout << "Could not read input image file: " << imgPath << endl;
        return -1;
    }

    cout << "image size x,y: " << img.cols << "," << img.rows << endl;

    //Threshold image
    Mat frameThresh;
    threshold(img, frameThresh, 130, 255, CV_THRESH_BINARY);

    if (!check_grid_dividers(img, gridWidth, gridHeight)) {
        cout << "ERROR" << endl;
        return -1;
    }

    // Fill foreground & grid with 0 ... 
    Mat imgResult = Mat::zeros(frameThresh.rows, frameThresh.cols, CV_8U);
    //Mat fgGrid = Mat::ones(div, div, CV_8U);
    Mat fgGrid = Mat::zeros(gridHeight, gridWidth, CV_8U);

    Mat tstImg = frameThresh.clone();
    fill_grid(tstImg, fgGrid);
    //_grid_fill(tstImg, fgGrid,gridWidth, gridHeight );

    fill_img_with_grid(fgGrid, imgResult);

    namedWindow("thresh", 1);
    imshow("thresh", frameThresh);
    imshow("result", imgResult);

    //    duration = (double) getTickCount() - duration;
    //    duration /= getTickFrequency(); // the elapsed time in s
    //    cout << "duration: " << duration * 1000 << " ms" << endl;
    //    waitKey(0);

    cout << "done" << endl;
    return 0;
}

