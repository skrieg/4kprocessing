/**
 * @brief parameters user interface
 * @file obj_detect_gui.cpp
 * @author sylvain.krieg@master.hes-so.ch
 * @date decembre 2015
 */

#include "obj_detect.h"

// Constructor

GuiParam::GuiParam(void) {
    /* ShadowActive = true;
     Mog2Variance_trkbar = 0; //280;
     Mog2Complexity_trkbar = 3;
     mixture_trkbar = 5;
     shadow_trkbar = 85;
     history_trkbar = 0;
     KnnDist2Threshold = 400;
     //KnnNSamples = 2;
     kNNSamples = 2;
     Mog2Ratio_trkbar = 54;
     dilateElmt_trkbar = 1;
     DilateActive = false;
     openActive = false;
     closElmt_trkbar = 3;
     opnElmt_trkbar = 1;
     closeActive = false;
     blobSizeMin_trkbar = 50;
     blobSizeMax_trkbar = 200;
     blobDetectActive = false;
     //trackingActive = false;
     objSizeMin_trkbar = 200;
     test1_trkbar = 1;
     contPoly_trkbar = 0;
     canny_t1 = 40;
     canny_t2 = 0;
     hough_line_min = 11;
     hough_accu_tresh = 12;
     hough_max_gap = 1;
     //contourDetectActive = false;
     //actualOperation = DO_NOTHING;
     //controur_trkbar = 0;
     //testActive = false;
     grillFillActive = true;
     //gridFille_trckbar = 0;
     holesFillActive = true;*/
}


//////////////////////////////////////////////////////////////////////////
// GUI Callback definition
//////////////////////////////////////////////////////////////////////////

void cb_void(int p1, void* p2) {
    //nothing to do with the trackbar
}

void cb_shadow_on(int p1, void* p2) {
    //param.ShadowActive = true;
    *(int*) p2 = true;
}

void cb_shadow_off(int p1, void* p2) {
    //param.ShadowActive = false;
    *(int*) p2 = false;
}

void cb_rad_srcRaw(int p1, void* p2) {
    //param.actualSource = SRC_RAW;
    *(int*) p2 = SRC_RAW;
}

void cb_rad_srcOp(int p1, void* p2) {
    //param.actualSource = SRC_MOG2;
    *(int*) p2 = SRC_PREPROC;
}

void cb_rad_SrcResult(int p1, void* p2) {
    //param.actualSource = SRC_SURROUND;
    *(int*) p2 = SRC_RESULT;
}

void cb_dilate_on(int p1, void* p2) {
    //param.ErosionActive = true;
    *(int*) p2 = true;
}

void cb_dilate_off(int p1, void* p2) {
    //param.ErosionActive = false;
    *(int*) p2 = false;
}

void cb_open_on(int p1, void* p2) {
    //param.closingActive = true;
    *(int*) p2 = true;
}

void cb_open_off(int p1, void* p2) {
    //param.closingActive = false;
    *(int*) p2 = false;
}

void cb_close_on(int p1, void* p2) {
    //param.closingActive = true;
    *(int*) p2 = true;
}

void cb_close_off(int p1, void* p2) {
    //param.closingActive = false;
    *(int*) p2 = false;
}

void cb_blob_det_on(int p1, void* p2) {
    //param.testActive = true;
    //cout << "on " << p1 << endl;
    *(int*) p2 = true;
}

void cb_blob_det_off(int p1, void* p2) {
    //testActive = false;
    //param.testActive = false;
    //cout << "off " << p1 << endl;
    *(int*) p2 = false;
}

void cb_test_on(int p1, void* p2) {
    //param.closingActive = true;
    *(int*) p2 = true;
}

void cb_test_off(int p1, void* p2) {
    //param.closingActive = false;
    *(int*) p2 = false;
}

void cb_tracking_on(int p1, void* p2) {
    *(int*) p2 = true;
}

void cb_tracking_off(int p1, void* p2) {
    *(int*) p2 = false;
}

void cb_flood_on(int p1, void* p2) {
    *(int*) p2 = true;
}

void cb_flood_off(int p1, void* p2) {
    *(int*) p2 = false;
}

void cb_grid_on(int p1, void* p2) {
    *(int*) p2 = true;
}

void cb_grid_off(int p1, void* p2) {
    *(int*) p2 = false;
}

void cb_holes_on(int p1, void* p2) {
    *(int*) p2 = true;
}

void cb_holes_off(int p1, void* p2) {
    *(int*) p2 = false;
}

void cb_save_settings(int p1, void* p2) {
    char cCurrentPath[PATH_MAX];
    if (getcwd(cCurrentPath, PATH_MAX) != 0) {
        std::string paramPath = std::string(cCurrentPath) + "/param.txt";
        cout << "save settings to : " << paramPath << endl;
        std::ofstream fp;
        fp.open(paramPath.c_str(), std::ofstream::out | std::ofstream::trunc); //trunc=clear file content
        if (!fp.is_open()) {
            perror("Error while opening log file");
        }
        GuiParam *gp = (GuiParam*) p2;

        fp << std::to_string(gp->Mog2Variance_trkbar) << endl;
        fp << std::to_string(gp->Mog2Ratio_trkbar) << endl;
        fp << std::to_string(gp->mixture_trkbar) << endl;
        fp << std::to_string(gp->Mog2Complexity_trkbar) << endl;
        fp << std::to_string(gp->ShadowActive) << endl;
        fp << std::to_string(gp->shadow_trkbar) << endl;
        fp << std::to_string(gp->history_trkbar) << endl;
        fp << std::to_string(gp->DilateActive) << endl;
        fp << std::to_string(gp->dilateElmt_trkbar) << endl;
        fp << std::to_string(gp->openActive) << endl;
        fp << std::to_string(gp->opnElmt_trkbar) << endl;
        fp << std::to_string(gp->closeActive) << endl;
        fp << std::to_string(gp->closElmt_trkbar) << endl;
        fp << std::to_string(gp->objSizeMin_trkbar) << endl;
        fp << std::to_string(gp->contPoly_trkbar) << endl;
        fp << std::to_string(gp->canny_t1) << endl;
        fp << std::to_string(gp->canny_t2) << endl;
        fp << std::to_string(gp->hough_line_min) << endl;
        fp << std::to_string(gp->hough_accu_tresh) << endl;
        fp << std::to_string(gp->hough_max_gap) << endl;
        fp << std::to_string(gp->gridFillActive) << endl;
        fp << std::to_string(gp->test1_trkbar) << endl;
        fp << std::to_string(gp->holesFillActive) << endl;
       
        fp.close();
    } else
        cout << "ERROR" << endl;
}

int read_param(ifstream& fd) {
    string buffer;
    getline(fd, buffer);
    return atoi(buffer.c_str());
}

void createGUI(string windowName, GuiParam* p) {
    //read parameters file
    cout << "guiparam read parameters file" << endl;
    char cCurrentPath[PATH_MAX];
    if (getcwd(cCurrentPath, PATH_MAX) != 0) {

        std::string lineParam;
        std::string paramPath = std::string(cCurrentPath) + "/param.txt";
        cout << "param file: " << paramPath << endl;
        ifstream infile;
        infile.open(paramPath);
        if (!infile.is_open()) {
            perror("Error while opening log file");
        }

        p->Mog2Variance_trkbar = read_param(infile);
        p->Mog2Ratio_trkbar = read_param(infile);
        p->mixture_trkbar = read_param(infile);
        p->Mog2Complexity_trkbar = read_param(infile);
        p->ShadowActive = read_param(infile);
        p->shadow_trkbar = read_param(infile);
        p->history_trkbar = read_param(infile);
        p->DilateActive = read_param(infile);
        p->dilateElmt_trkbar = read_param(infile);
        p->openActive = read_param(infile);
        p->opnElmt_trkbar = read_param(infile);
        p->closeActive = read_param(infile);
        p->closElmt_trkbar = read_param(infile);
        p->objSizeMin_trkbar = read_param(infile);
        p->contPoly_trkbar = read_param(infile);
        p->canny_t1 = read_param(infile);
        p->canny_t2 = read_param(infile);
        p->hough_line_min = read_param(infile);
        p->hough_accu_tresh = read_param(infile);
        p->hough_max_gap = read_param(infile);
        p->gridFillActive = read_param(infile);
        p->test1_trkbar = read_param(infile);
        p->holesFillActive = read_param(infile);



        infile.close();
        cout << "done." << endl;
    } else
        cout << "ERROR, param file not found" << endl;



    //Source selection
    //    createButton("src_raw", cb_rad_srcRaw, &p->actualSource, CV_RADIOBOX, 1);
    //    createButton("src_process", cb_rad_srcOp, &p->actualSource, CV_RADIOBOX, 0);
    //    createButton("src_result", cb_rad_SrcResult, &p->actualSource, CV_RADIOBOX, 0);
    cvCreateButton("saveSettings", cb_save_settings, p, CV_PUSH_BUTTON, 0);
    //MOG2 variance    
    createTrackbar("variance", NULL, &p->Mog2Variance_trkbar, 400, cb_void);
    //MOG2 ration
    createTrackbar("ratio", NULL, &p->Mog2Ratio_trkbar, 200, cb_void);
    createTrackbar("mixture", NULL, &p->mixture_trkbar, 100, cb_void);
    createTrackbar("complexity", NULL, &p->Mog2Complexity_trkbar, 100, cb_void);

    //MOG2 shadow
    createButton("shadow_on", cb_shadow_on, &p->ShadowActive, CV_RADIOBOX, p->ShadowActive);
    createButton("shadow_off", cb_shadow_off, &p->ShadowActive, CV_RADIOBOX, !p->ShadowActive);
    createTrackbar("Shadow", NULL, &p->shadow_trkbar, 100, cb_void);
    createTrackbar("History", NULL, &p->history_trkbar, 1000, cb_void);

    //Knn background subtraction
    createTrackbar("KnnTresh", NULL, &p->KnnDist2Threshold, 2000, cb_void);
    //createTrackbar("Knn nSamples", NULL, &p->KnnNSamples, 100, cb_void);
    createTrackbar("Knn samples", NULL, &p->kNNSamples, 10, cb_void);
    //Erosion
    createButton("dilate_on", cb_dilate_on, &p->DilateActive, CV_RADIOBOX, p->DilateActive);
    createButton("dilate_off", cb_dilate_off, &p->DilateActive, CV_RADIOBOX, !p->DilateActive);
    createTrackbar("dilate_elmt_size", NULL, &p->dilateElmt_trkbar, 30, cb_void);
    //Closing/opening
    createButton("opening_on", cb_open_on, &p->openActive, CV_RADIOBOX, p->openActive);
    createButton("opening_off", cb_open_off, &p->openActive, CV_RADIOBOX, !p->openActive);
    createTrackbar("opening_elmt_size", NULL, &p->opnElmt_trkbar, 30, cb_void);
    createButton("closing_on", cb_close_on, &p->closeActive, CV_RADIOBOX, p->closeActive);
    createButton("closing_off", cb_close_off, &p->closeActive, CV_RADIOBOX, !p->closeActive);
    createTrackbar("closing_elmt_size", NULL, &p->closElmt_trkbar, 30, cb_void);
    //Blob detection
    //createButton("blobDetect_ON", cb_blob_det_on, &p->blobDetectActive, CV_RADIOBOX, 0);
    //createButton("blobDetect_OFF", cb_blob_det_off, &p->blobDetectActive, CV_RADIOBOX, 0);
    //createTrackbar("blob_Min", NULL, &p->blobSizeMin_trkbar, 10000, cb_void);
    //createTrackbar("blob_Max", NULL, &p->blobSizeMax_trkbar, 30000, cb_void);
    //Tracking
    //createButton("Tracking_ON", cb_tracking_on, &p->trackingActive, CV_RADIOBOX, 0);
    //createButton("Tracking_OFF", cb_tracking_off, &p->trackingActive, CV_RADIOBOX, 1);
    //Contour detection
    createTrackbar("obj_size_min", NULL, &p->objSizeMin_trkbar, 10000, cb_void);
    //createTrackbar("test1", NULL, &p->test1_trkbar, 100, cb_void);
    createTrackbar("contPoly", NULL, &p->contPoly_trkbar, 100, cb_void);

    //Canny detector
    createTrackbar("canny thresh1", NULL, &p->canny_t1, 100, cb_void);
    createTrackbar("canny thresh2", NULL, &p->canny_t2, 100, cb_void);

    //Hough 
    createTrackbar("hough min size", NULL, &p->hough_line_min, 100, cb_void);
    createTrackbar("hough accu tresh", NULL, &p->hough_accu_tresh, 100, cb_void);
    createTrackbar("hough max gap", NULL, &p->hough_max_gap, 10, cb_void);

    //createTrackbar("contour", NULL, &p->controur_trkbar, 100, cb_void);
    //createButton("contours_ON", cb_contours_on, &p->contourDetectActive, CV_RADIOBOX, 0);
    //createButton("contours_OFF", cb_contours_off, &p->contourDetectActive, CV_RADIOBOX, 0);


    createButton("gridFill_ON", cb_grid_on, &p->gridFillActive, CV_RADIOBOX, p->gridFillActive);
    createButton("gridFill_OFF", cb_grid_off, &p->gridFillActive, CV_RADIOBOX, !p->gridFillActive);
    createTrackbar("test", NULL, &p->test1_trkbar, 100, cb_void);
    createButton("holesFill_ON", cb_grid_on, &p->holesFillActive, CV_RADIOBOX, p->holesFillActive);
    createButton("holesFill_OFF", cb_grid_off, &p->holesFillActive, CV_RADIOBOX, !p->holesFillActive);



    //Operation selection
    //    createButton("detect_contour", cb_op_contour, &p->actualOperation, CV_RADIOBOX, 0);
    //    createButton("MOG", cb_op_mog, &p->actualOperation, CV_RADIOBOX, 0);
    //    createButton("OFF", cb_op_no, &p->actualOperation, CV_RADIOBOX, 1);


    //Test elements
    // createButton("testON", cb_test_on, &p->testActive, CV_RADIOBOX, 0);
    // createButton("testOFF", cb_test_off, &p->testActive, CV_RADIOBOX, 1);
}

