/**
 * @brief Program using multicore for objects classification
 * @file classification.cpp
 * @author sylvain.krieg@master.hes-so.ch
 * @date decembre 2015
 */

#include "obj_detect.h"
#include "Log.h"
#include <pthread.h>
#include <thread>

//#include "cudabgsegm.hpp" //cuda bgsub

using namespace cv;
using namespace std;
//using namespace Core;
GuiParam param_multi;
int paused_multi = 0;

#define NB_INTERPROCESS_BUFFER 4 

/**
 * 
 * @param wI
 * @param rI
 * @return 
 */
int checkAvail(int* wI, int* rI) {
    int avail = 0;
    if (*wI < *rI)
        avail = (*rI - *wI) - 1;
    else
        avail = ((RING_BUFFER_SIZE - 1) - *wI + (*rI - 1));
    return avail;
}

bool isFull(int* wI, int* rI) {
    int avail = checkAvail(wI, rI);
    if (avail == 0)
        return true;
    else
        return false;
}

bool isEmpty(int* wI, int* rI) {
    int avail = (RING_BUFFER_SIZE - 2) - checkAvail(wI, rI);
    if (avail == 0)
        return true;
    else
        return false;
}

void task_capture(const char* path, bool* flag, pthread_mutex_t* mtex, imgRingBuffer* ringBuff, int* count) {

    cout << "prod path" << path << endl;
    VideoCapture capture = openVid(path);

    //read first image to get the image size
    Mat localMat;
    Mat copyMat;
    if (!capture.read(localMat)) {
        cerr << "Unable to read next frame." << endl;
        cerr << "Exiting..." << endl;
        exit(EXIT_FAILURE);
    }

    int imgSize = localMat.total() * localMat.elemSize(); //imgIn.total() * imgIn.elemSize();
    cout << "CAPT: enabled" << endl;
    string txtImg;
    int cpt = 0;
    while (*flag == false) {
        pthread_mutex_lock(&ringBuff->mtx);
        if (!isFull(&(ringBuff->wrIndex), &(ringBuff->rdIndex))) {
            if (!capture.read(localMat)) {
                cerr << "Unable to read next frame." << endl;
                cerr << "Exiting..." << endl;
                exit(EXIT_FAILURE);
            }

            copyMat = localMat.clone();
            memcpy((char*) ((char*) (&ringBuff->dataImageschar) + (int) (*ringBuff).wrIndex * imgSize), localMat.data, imgSize); // memcpy((char*) (sharedBuffer + (int) *wrIndex * imgSize), localMat.data, imgSize);
            *count = cpt;
            cpt++;

            (*ringBuff).wrIndex++;
            if ((*ringBuff).wrIndex == RING_BUFFER_SIZE)
                (*ringBuff).wrIndex = 0;
            //cout << "CAPT: counter " << cpt << endl;
        } else {//shared buffer full
            //cout << "CAPT: output buffer full" << endl;
            usleep(20 MS);
        }
        pthread_mutex_unlock(&ringBuff->mtx);

        //        imshow("out", copyMat);
        //        waitKey(1);

        if (cpt == 1000) {
            cout << "CAPT: done, cpt == " << cpt << endl;
            usleep(20 MS);
            *flag = true;
        }
    }
    cout << "----------produce:: end, flag is true" << endl;
}

void task_back_ground_sub(int imgRows, int imgCols, int imgType, int imgSize, bool* flag, pthread_mutex_t* mtex, imgRingBuffer* ringBufferIN, imgRingBuffer* ringBufferOUT, int* count) {
    cout << "BGSUB: enabled" << endl;
    Mat imgFromBuffer(imgRows, imgCols, imgType, Scalar(0, 0, 0));
    Mat fgFrame; //(imgRows, imgCols, imgType, Scalar(0, 0, 255));
    Ptr<BackgroundSubtractorMOG2> pSub;
    pSub = createBackgroundSubtractorMOG2(); //MOG2 approach
    int cpt = 0;
    while (!*flag) {
        //cout << "BGSUB: wait lock in" << endl;
        pthread_mutex_lock(&ringBufferIN->mtx);
        //cout << "BGSUB: lock in OK" << endl;
        if (!isEmpty(&(ringBufferIN->wrIndex), &(ringBufferIN->rdIndex))&& !isFull(&(ringBufferOUT->wrIndex), &(ringBufferOUT->rdIndex))&&*flag == false) {
            memcpy(imgFromBuffer.data, (char*) ((char*) (&ringBufferIN->dataImageschar) + (int) (*ringBufferIN).rdIndex * imgSize), imgSize);
            (*ringBufferIN).rdIndex++; //(*rdIndex)++;
            if ((*ringBufferIN).rdIndex == RING_BUFFER_SIZE)
                (*ringBufferIN).rdIndex = 0;
            pthread_mutex_unlock(&ringBufferIN->mtx);

            backGroundSubMog2(param_multi, imgFromBuffer, fgFrame, pSub, paused_multi);
            cv::threshold(fgFrame, fgFrame, 254, 255, cv::THRESH_BINARY); //remove shadow 
            pthread_mutex_lock(&ringBufferOUT->mtx);
            memcpy((char*) ((char*) (&ringBufferOUT->dataImageschar) + (int) (*ringBufferOUT).wrIndex * imgSize), fgFrame.data, imgSize / 3); // memcpy((char*) (sharedBuffer + (int) *wrIndex * imgSize), localMat.data, imgSize);

            (*ringBufferOUT).wrIndex++;
            if ((*ringBufferOUT).wrIndex == RING_BUFFER_SIZE)
                (*ringBufferOUT).wrIndex = 0;
            // cout << "bgsub ---------------> " << " wrIdx:" << ringBufferOUT->wrIndex << " rdIdx:" << ringBufferOUT->rdIndex << endl;
            pthread_mutex_unlock(&ringBufferOUT->mtx);
            //cout << "BGSUB: counter = " << cpt++ << endl;
            //cout << "bgsub " << cpt++ << " wr:" << ringBufferOUT->wrIndex << " rd:" << ringBufferOUT->rdIndex << endl;
        } else { //buffer empty
            pthread_mutex_unlock(&ringBufferIN->mtx);
            usleep(60 MS);
        }
    }
    cout << "----------BGSUB::: end, flag is true" << endl;
}

void task_grid(int imgRows, int imgCols, int imgType, int imgSize, bool* flag, pthread_mutex_t* mtex, imgRingBuffer* ringBufferIN, imgRingBuffer* ringBufferOUT, int* count) {
    cout << "GRID: process enabled" << endl;
    //input: grayscale image full size, image rows, cols, ringbuffIN, ringBuffOUT
    //output: grayscale image full size 

    Mat imgFromBuffer(imgRows, imgCols, CV_8U, Scalar(0, 0, 0));
    Mat imgToBuffer(imgRows, imgCols, CV_8U, Scalar(0, 0, 0));
    Ptr<BackgroundSubtractorMOG2> pSub;
    pSub = createBackgroundSubtractorMOG2(); //MOG2 approach
    Mat fgGrid;
    int cnt = 0;
    while (!*flag) {

        pthread_mutex_lock(&ringBufferIN->mtx);

        //cout<<"ok "<<getpid()<<endl;
        if (!isEmpty(&(ringBufferIN->wrIndex), &(ringBufferIN->rdIndex))&& !isFull(&(ringBufferOUT->wrIndex), &(ringBufferOUT->rdIndex))&&*flag == false) {
            //cout << "grid1: " << getpid() << " cpt" << cnt << " wr:" << ringBufferOUT->wrIndex << " rd:" << ringBufferOUT->rdIndex << " full?:" << isFull(&(ringBufferOUT->wrIndex), &(ringBufferOUT->rdIndex)) << endl;
            memcpy(imgFromBuffer.data, (char*) ((char*) (&ringBufferIN->dataImageschar) + (int) (*ringBufferIN).rdIndex * imgSize), imgSize / 3);
            (*ringBufferIN).rdIndex++; //(*rdIndex)++;
            if ((*ringBufferIN).rdIndex == RING_BUFFER_SIZE)
                (*ringBufferIN).rdIndex = 0;
            pthread_mutex_unlock(&ringBufferIN->mtx);

            //Operations start
            const int boxSizeX = 4;
            const int boxSizeY = 4;
            int gridW = imgCols / boxSizeX; //320;256
            int gridH = imgRows / boxSizeY; //180;144
            if (!check_grid_dividers(imgFromBuffer, gridW, gridH))
                exit(EXIT_FAILURE);
            fgGrid = Mat::zeros(gridH, gridW, CV_8U);
            fill_grid(imgFromBuffer, fgGrid); //in,out
            imgToBuffer = Mat::zeros(imgRows, imgCols, CV_8U);

            fill_img_with_grid(fgGrid, imgToBuffer); //out,in
            //pthread_mutex_lock(&ringBufferOUT->mtx);
            memcpy((char*) ((char*) (&ringBufferOUT->dataImageschar) + (int) (*ringBufferOUT).wrIndex * imgSize), imgToBuffer.data, imgSize / 3); // memcpy((char*) (sharedBuffer + (int) *wrIndex * imgSize), localMat.data, imgSize);

            (*ringBufferOUT).wrIndex++;
            if ((*ringBufferOUT).wrIndex == RING_BUFFER_SIZE)
                (*ringBufferOUT).wrIndex = 0;
            ///cout << "GRID: ---------------> " << " wrIdx:" << ringBufferOUT->wrIndex << " rdIdx:" << ringBufferOUT->rdIndex << endl;
            pthread_mutex_unlock(&ringBufferOUT->mtx);

        } else { //buffer empty
            /*if (isEmpty(&(ringBufferIN->wrIndex), &(ringBufferIN->rdIndex)))
                 cout << "GRID: bufferIN empty" << endl;
             else if (isFull(&(ringBufferOUT->wrIndex), &(ringBufferOUT->rdIndex)))
                 cout << "GRID: bufferOUT full" << endl;
             */
            pthread_mutex_unlock(&ringBufferIN->mtx);
            usleep(20 MS);
        }
        //        usleep(40 MS);
    }
    cout << "----------grid:: end, flag is true " << getpid() << endl;
}

void task_detect(int imgRows, int imgCols, int imgType, int imgSize, bool* flag, imgRingBuffer* ringBufferIN, imgRingBuffer* ringBufferOUT, int* count) {
    cout << "detect process enabled" << endl;

    Mat imgFromBuffer(imgRows, imgCols, CV_8U, Scalar(0, 0, 0));
    //Mat imgToBuffer(imgRows, imgCols, CV_8U, Scalar(0, 0, 0));
    Mat structuringElement;
    Mat imgToBuffer(imgRows, imgCols, imgType, Scalar(0, 0, 0));
    int cpt = 0;
    while (!*flag) {
        pthread_mutex_lock(&ringBufferIN->mtx);
        if (!isEmpty(&(ringBufferIN->wrIndex), &(ringBufferIN->rdIndex))&& !isFull(&(ringBufferOUT->wrIndex), &(ringBufferOUT->rdIndex))&&*flag == false) {
            memcpy(imgFromBuffer.data, (char*) ((char*) (&ringBufferIN->dataImageschar) + (int) (*ringBufferIN).rdIndex * imgSize), imgSize / 3);
            (*ringBufferIN).rdIndex++; //(*rdIndex)++;
            if ((*ringBufferIN).rdIndex == RING_BUFFER_SIZE)
                (*ringBufferIN).rdIndex = 0;
            pthread_mutex_unlock(&ringBufferIN->mtx);

            //Operations start

            //Erosion
                   structuringElement = getStructuringElement(MORPH_RECT, Size(param_multi.dilateElmt_trkbar + 1, 8 * param_multi.dilateElmt_trkbar + 1),
                           Point(param_multi.dilateElmt_trkbar, param_multi.dilateElmt_trkbar));
                 //  erode(imgFromBuffer, imgFromBuffer, structuringElement);
                   //Detection
                   cvtColor(imgFromBuffer, imgToBuffer, CV_GRAY2BGR); //<-- cette ligne empeche le waitpid
                   obj_detection(imgFromBuffer, imgToBuffer); //src, dest
                   //Operations end
             
           // cvtColor(imgFromBuffer, imgToBuffer, CV_GRAY2BGR); //<-- cette ligne empeche le waitpid

            pthread_mutex_lock(&ringBufferOUT->mtx);
            memcpy((char*) ((char*) (&ringBufferOUT->dataImageschar) + (int) (*ringBufferOUT).wrIndex * imgSize), imgToBuffer.data, imgSize); // memcpy((char*) (sharedBuffer + (int) *wrIndex * imgSize), localMat.data, imgSize);

            (*ringBufferOUT).wrIndex++;
            if ((*ringBufferOUT).wrIndex == RING_BUFFER_SIZE)
                (*ringBufferOUT).wrIndex = 0;
            // cout << "detect " << cpt++ << " wr:" << ringBufferOUT->wrIndex << " rd:" << ringBufferOUT->rdIndex << endl;
            pthread_mutex_unlock(&ringBufferOUT->mtx);

        } else { //buffer empty
            /*   if (isEmpty(&(ringBufferIN->wrIndex), &(ringBufferIN->rdIndex)))
                   cout << "detect: buffer empty" << endl;
               else if (isFull(&(ringBufferOUT->wrIndex), &(ringBufferOUT->rdIndex)))
                   cout << "detect: bg sub outputbuff full" << endl;
             */ pthread_mutex_unlock(&ringBufferIN->mtx);
            usleep(20 MS);
        }
        //        usleep(40 MS);
    }

    cout << "----------detect:: end, flag is true" << endl;
}

void task_show(const char* logPath, int imgRows, int imgCols, int imgType, int imgSize, bool* flag, imgRingBuffer* ringBufferIN) {
    cout << "show process enabled" << endl;
    Mat imgOut(imgRows, imgCols, imgType, Scalar(0, 0, 0));
    Mat resized;
    double durationPrev = (double) getTickCount();
    double duration;
    usleep(100 MS);

    //const string logPath = "/home/sylvain/tm/bg_sub_nb/log/div6.txt";
    Log::clearFile(logPath);
    int cpt = 0;
    while (!*flag) {
        pthread_mutex_lock(&ringBufferIN->mtx);
        if (!isEmpty(&(ringBufferIN->wrIndex), &(ringBufferIN->rdIndex)) &&*flag == false) {

            duration = (double) getTickCount() - durationPrev;
            durationPrev = (double) getTickCount();

            memcpy(imgOut.data, (char*) ((char*) (&ringBufferIN->dataImageschar) + (int) (*ringBufferIN).rdIndex * imgSize), imgSize);
            (*ringBufferIN).rdIndex++; //(*rdIndex)++;
            if ((*ringBufferIN).rdIndex == RING_BUFFER_SIZE)
                (*ringBufferIN).rdIndex = 0;

            //Operations
            resize(imgOut, resized, Size(640, 480), 0, 0, INTER_LINEAR); //--->! empeche la teche de se terminer
            //imshow("out", resized);
            imshow("ou__t", imgOut);
            waitKey(1);
            stringstream logStream;
        } else {
            //  cout << "show: buffer empty" << endl;
            usleep(20 MS);
        }
        pthread_mutex_unlock(&ringBufferIN->mtx);

    }
    cout << "----------show:: end, flag is true" << endl;
}


string type2str(int type) {
    string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch (depth) {
        case CV_8U: r = "8U";
            break;
        case CV_8S: r = "8S";
            break;
        case CV_16U: r = "16U";
            break;
        case CV_16S: r = "16S";
            break;
        case CV_32S: r = "32S";
            break;
        case CV_32F: r = "32F";
            break;
        case CV_64F: r = "64F";
            break;
        default: r = "User";
            break;
    }

    r += "C";
    r += (chans + '0');

    return r;
}

/**
 * Main application. Capture video and apply different operations using process.
 * @param videoFilename
 */
void multiTask(const char* videoFilename, const char* logPath) {

    Mat inImg;
    VideoCapture capture = openVid(videoFilename);
    capture.read(inImg);
    cout << "img type:" << type2str(inImg.type());
    capture.release(); //close (a task will access later from another mem space)

    String windowName1 = "visual1";
    namedWindow(windowName1, WINDOW_AUTOSIZE);
    createGUI(windowName1, &param_multi);

    int imgSize = inImg.total() * inImg.elemSize();
    cout << " image size::" << imgSize << " total:" << inImg.total() << " elmt size:" << inImg.elemSize() << endl;
    int handle = shm_open("/shm", O_CREAT | O_RDWR, 0777);
    int memorySize = sizeof (bool) + sizeof (int) + sizeof (pthread_mutex_t) + 4 * sizeof (int) + 2 * sizeof (pthread_mutex_t)+(RING_BUFFER_SIZE) * imgSize * 4;
    ftruncate(handle, memorySize);
    char *memory = (char *) mmap(0, memorySize, PROT_READ | PROT_WRITE, MAP_SHARED, handle, 0);
    char *memAddress = (char*) memory;
    cout << "addr base  :" << (int*) memAddress << endl;
    //shared memory flag
    bool *sharedFlag = (bool*)memAddress; //(bool*) (memory);
    memAddress = (char*) (memAddress + sizeof (bool));
    cout << "addr count :" << (int*) memAddress << endl;
    //shared memory variable
    int *count = (int*) (memAddress); //(memory + sizeof (bool) + sizeof (pthread_mutex_t));
    memAddress = (char*) (memAddress + sizeof (int));
    *count = 0;
    *sharedFlag = false;
    cout << "addr mutex :" << (int*) memAddress << endl;


    //shared memory mutex 12
    pthread_mutexattr_t attributes;
    pthread_mutexattr_init(&attributes);
    pthread_mutexattr_setpshared(&attributes, PTHREAD_PROCESS_SHARED);
    pthread_mutex_t *mtex = (pthread_mutex_t*) (memAddress); // (memory + imgSize + sizeof (bool));
    pthread_mutex_init(mtex, &attributes);
    pthread_mutexattr_destroy(&attributes);

    memAddress = (char*) (memAddress + sizeof (pthread_mutex_t));
    cout << "addr buff12:" << (int*) memAddress << endl;

    //Ring buffer capture-->bgsub
    imgRingBuffer *buff12;
    buff12 = (imgRingBuffer*) memAddress; //(memory + sizeof (bool) + sizeof (pthread_mutex_t) + sizeof (int));
    buff12->wrIndex = 0;
    buff12->rdIndex = 0;
    buff12->mtx = PTHREAD_MUTEX_INITIALIZER;
    memAddress = (char*) (memAddress + 2 * sizeof (int) + sizeof (pthread_mutex_t));
    buff12->dataImageschar = (char*) (memAddress); //(memory + sizeof (bool) + sizeof (pthread_mutex_t) + sizeof (int) + 2 * sizeof (int));
    memAddress = (char*) (memAddress + RING_BUFFER_SIZE * imgSize);

    //Ring buffer bgSub-->gridFill
    cout << "addr buff23:" << (int*) memAddress << endl;
    imgRingBuffer *buff23;
    buff23 = (imgRingBuffer*) memAddress;
    buff23->wrIndex = 0;
    buff23->rdIndex = 0;
    buff23->mtx = PTHREAD_MUTEX_INITIALIZER;
    memAddress = (char*) (memAddress + 2 * sizeof (int) + sizeof (pthread_mutex_t));
    buff23->dataImageschar = (char*) (memAddress);
    memAddress = (char*) (memAddress + RING_BUFFER_SIZE * imgSize);

    //Ring buffer gridFill-->detect
    cout << "addr buff34:" << (int*) memAddress << endl;
    imgRingBuffer *buff34;
    buff34 = (imgRingBuffer*) memAddress;
    buff34->wrIndex = 0;
    buff34->rdIndex = 0;
    buff34->mtx = PTHREAD_MUTEX_INITIALIZER;
    memAddress = (char*) (memAddress + 2 * sizeof (int) + sizeof (pthread_mutex_t));
    buff34->dataImageschar = (char*) (memAddress);
    memAddress = (char*) (memAddress + RING_BUFFER_SIZE * imgSize);

    //Ring buffer detect -->show
    cout << "addr buff45:" << (int*) memAddress << endl;
    imgRingBuffer *buff45;
    buff45 = (imgRingBuffer*) memAddress;
    buff45->wrIndex = 0;
    buff45->rdIndex = 0;
    buff45->mtx = PTHREAD_MUTEX_INITIALIZER;
    memAddress = (char*) (memAddress + 2 * sizeof (int) + sizeof (pthread_mutex_t));
    buff45->dataImageschar = (char*) (memAddress);
    memAddress = (char*) (memAddress + RING_BUFFER_SIZE * imgSize);


    cout << "mem end    :" << (int*) memAddress << endl;

    //Check if used memory is not bigger than allocated memory
    cout << "memory size: " << memorySize << endl;
    if ((int*) memAddress > ((int*) memory) + memorySize) {
        cout << "ERROR, used memory > allocated memory: " << (int*) memAddress << " " << (int*) memory + memorySize << endl;
        exit(EXIT_FAILURE);
    } else
        cout << "OK, used memory <= allocated memory: " << (int*) memAddress << " " << ((int*) memory) + memorySize << endl;
    // printf("OK;mem used: %X \n", (int*)memory);
    cout << "sizeof int:" << sizeof (int) << " char:" << sizeof (char) << " mtx:" << sizeof (pthread_mutex_t) << " img:" << imgSize << endl;


    //1:capture, 2:bgsub, 3:grid, 4:detect, 5:grid, 6:grid, parent:show

    pid_t pid1, pid2, pid3, pid4, pid5, pid6;
    pid1 = fork();
    if (pid1 == 0) { // child process Capture
        printf("child process 1 (get image)\n");
        task_capture(videoFilename, sharedFlag, mtex, buff12, count); //&buff12->wrIndex, &buff12->rdIndex, (buff12->dataImageschar));
        usleep(200 MS);
        printf("********child process 1 (get image)end\n");
        exit(0);
    } else if (pid1 > 0) { //parent process 
        pid2 = fork();
        if (pid2 == 0) {//child 2
            printf("child process 2 (bgsub)\n");
            //procBackGroundSub(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, mtex, buff12, buff23, count); //wrIndex, rdIndex, sharedBuffer);
            task_back_ground_sub(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, mtex, buff12, buff23, count);

            printf("********child process 2 (bgsub) done\n");
            exit(0);
        } else if (pid2 > 0) {
            pid3 = fork();
            if (pid3 == 0) {//child 3
                printf("child process 3 (fillgrid)\n");
                task_grid(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, mtex, buff23, buff34, count);
                printf("********child process 3 (fillgrid)end %d\n", getpid());
                exit(0);
            } else if (pid3 > 0) {//parent
                pid4 = fork();
                if (pid4 == 0) {
                    printf("child process 4 (detect)\n");
                    task_detect(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, buff34, buff45, count);
                    printf("********child process 4 (detect)end\n");
                    exit(0);
                } else if (pid4 > 0) {//parent
                    //                    pid5 = fork();
                    //                    if (pid5 == 0) {
                    //                        printf("child process 5 (fillgrid)\n");
                    //                        task_grid(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, mtex, buff23, buff34, count);
                    //                        printf("********child process 5 (fillgrid)end %d\n", getpid());
                    //                        exit(0);
                    //                    } else if (pid5 > 0) {//parent
                    //                        pid6 = fork();
                    //                        if (pid6 == 0) {
                    //                            printf("child process 6 (fillgrid)\n");
                    //                            task_grid(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, mtex, buff23, buff34, count);
                    //                            printf("********child process 6 (fillgrid)end %d\n", getpid());
                    //                            exit(0);
                    //                        } else if (pid6 > 0) {
                    int status;
                    printf("parent process\n");
                    Mat imgOut(inImg.rows, inImg.cols, inImg.type(), Scalar(0, 0, 255));

                    //task_show(inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, buff23);
                    task_show(logPath, inImg.rows, inImg.cols, inImg.type(), imgSize, sharedFlag, buff45);

                    cout << "********parent end" << endl;

                    cout << "wait 1: capt" << endl;
                    waitpid(pid1, NULL, 0); //wait child to finish
                    cout << "wait 2: bgsub" << endl;
                    waitpid(pid2, NULL, 0); //wait child to finish
                    cout << "wait 3: grid " << endl;
                    waitpid(pid3, NULL, 0);
                    cout << "wait 4: detect ! pid4 isn't terminated!" << endl;
                    waitpid(pid4, NULL, 0);
                    cout << "wait 5: grid" << endl;
                    //waitpid(pid5, NULL, 0);
                    cout << "wait 6: show" << endl;
                    //waitpid(pid6, NULL, 0);
                    cout << "..." << endl;
                    //free shared memory
                    munmap(memory, memorySize);
                    shm_unlink("/shm");
                    //                        } else {
                    //                            printf("fork(6) failed!\n");
                    //                        }
                    //                    } else {
                    //                        printf("fork(5) failed!\n");
                    //                    }
                } else {
                    printf("fork(4) failed!\n");
                }
            } else {
                printf("fork(3) failed!\n");
            }
        } else {//fork failed
            printf("fork(2) failed!\n");
        }
    } else { // fork failed
        printf("fork(1) failed!\n");

    }


}
