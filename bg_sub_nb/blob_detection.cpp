#include "obj_detect.h"



SimpleBlobDetector::Params pBLOB;

void blob_detector_config(GuiParam* p) {
    pBLOB.thresholdStep = 10;
    pBLOB.minThreshold = 10;
    pBLOB.maxThreshold = 220;
    pBLOB.minRepeatability = 2;
    pBLOB.minDistBetweenBlobs = 10;
    pBLOB.filterByColor = false;
    pBLOB.blobColor = 0;
  //  pBLOB.filterByArea = false;
    pBLOB.minArea = p->blobSizeMin_trkbar; //param.blobSizeMin_trkbar;
    pBLOB.maxArea = p->blobSizeMax_trkbar; //param.blobSizeMax_trkbar;
    pBLOB.filterByCircularity = false;
    pBLOB.minCircularity = 0.9f;
    pBLOB.maxCircularity = (float) 1e37;
    pBLOB.filterByInertia = false;
    pBLOB.minInertiaRatio = 0.1f;
    pBLOB.maxInertiaRatio = (float) 1e37;
    pBLOB.filterByConvexity = false;
    pBLOB.minConvexity = 0.95f;
    pBLOB.maxConvexity = (float) 1e37;
    pBLOB.filterByArea = true;
}

vector<KeyPoint> blob_detection(Mat img, GuiParam* p) {
    Mat result;
    pBLOB.minArea = p->blobSizeMin_trkbar; //param.blobSizeMin_trkbar;
    pBLOB.maxArea = p->blobSizeMax_trkbar; //param.blobSizeMax_trkbar;
    Ptr<Feature2D> b;
    vector<KeyPoint> keyImg;
    b = SimpleBlobDetector::create(pBLOB);
    Ptr<SimpleBlobDetector> sbd = b.dynamicCast<SimpleBlobDetector>();
    sbd->detect(img, keyImg, 0);
    return keyImg;
}



void obj_detection(const Mat& sourceImage, Mat& detectionFrame) {
    vector<vector<Point> > contours; //contain points from contour detection
    vector<Vec4i> hierarchyContour; //Vec4i : [Next, Previous, First_Child, Parent]
    //int thresh = 100;
    //int max_thresh = 255;
    Mat threshold_output;
    Mat fgImg;
    //int _levels = 10;
    int nbObj = 0;

    fgImg = sourceImage.clone();
    //RETR_EXTERNAL, RETR_TREE, .. CV_CHAIN_APPROX_SIMPLE CHAIN_APPROX_TC89_L1
    findContours(fgImg, contours, hierarchyContour, RETR_TREE, CHAIN_APPROX_TC89_L1, Point(0, 0));
    //drawContours(detectionFrame, contours, _levels <= 0 ? 3 : -1, Scalar(0, 55, 255), 1, LINE_AA, hierarchyContour, std::abs(_levels));
    //drawContours(detectionFrame, contours, _levels <= 0 ? 3 : -1, Scalar(0, 55, 255), CV_FILLED, LINE_AA, hierarchyContour, std::abs(_levels));

    /// Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point> > contours_poly(contours.size());
    vector<Rect> boundRect(contours.size());

    for (unsigned int i = 0; i < contours.size(); i++) {
        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contours_poly[i]));
        if (hierarchyContour[i][3] == -1) {
            //            if (contourArea(contours[i]) < 400.0)//small items
            //                //rectangle(detectionFrame, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 0, 255), 2, 8, 0);
            //                NULL;
            //            else {
            //                rectangle(detectionFrame, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 2, 8, 0);
            //                nbObj++;
            //            }
            if (contourArea(contours[i]) > 400.0) {
                rectangle(detectionFrame, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 2, 8, 0);
                nbObj++;
            }
        }
    }
}



