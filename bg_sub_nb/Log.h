/*
 * Log.h
 *
 *  Created on: Aug 9, 2015
 *      Author: jonathan
 */

#include <string>
#include <sstream>

#include <stdint.h>

#ifndef LOG_LOG_H_
#define LOG_LOG_H_

//namespace Core {

    class Log {
    public:

        typedef enum E_LOG_TYPE {
            VERBOSE,
            INFO,
            LOG,
            ERROR,
            CRITICAL
        } e_log_type;

    public:
        Log();
        virtual ~Log();
        
        static void write(std::string, std::string);
         static void write(e_log_type, std::string, std::string);
        //static void write (std::string, int, int, uint8_t *);
        static void clearFile(std::string dir);
        static void setLevel(e_log_type);

        static std::string getLastError();
        static void setDirectory(std::string);

    private:
        static std::string getTimestamp();

        static std::string getLevel(e_log_type);

        static e_log_type type;

        static std::string directory;

    };

//} /* namespace Sensiwall */

#endif /* LOG_LOG_H_ */
