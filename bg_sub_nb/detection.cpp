/**
 * @brief Object detection by contour
 * @file detection.cpp
 * @author sylvain.krieg@master.hes-so.ch
 * @date decembre 2015
 */

#include "obj_detect.h"


/**
 * Detect objet contour
 * @param param guiparam
 * @param fgFrame foreground frame like background subraction result
 * @param colorFrameIn video frame
 * @param detectionColorMask output frame, contains detected objects
 * @param actorsContour
 * @param actorsRectangle bounding rectangle arround the object
 */
void detect_contours(GuiParam param, const Mat fgFrame, const Mat colorFrameIn, Mat detectionColorMask, vector<vector<Point> >& actorsContour, vector<Rect> & actorsRectangle) {
    vector<vector<Point> > contours; //contain points from contour detection
    vector<Vec4i> hierarchyContour; //Vec4i : [Next, Previous, First_Child, Parent]
    Mat fgImg;
    int _levels = 10;
    int nbObj = 0;
    fgImg = fgFrame.clone();    //local copy
    findContours(fgImg, contours, hierarchyContour, RETR_EXTERNAL/*RETR_TREE*/, CHAIN_APPROX_TC89_L1, Point(0, 0));
    Mat detectMask = Mat::zeros(fgFrame.size(), fgFrame.type());

    // Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point> > contours_poly(contours.size());
    vector<Rect> boundRect(contours_poly.size());
    for (unsigned int i = 0; i < contours_poly.size(); i++) {
        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contours_poly[i]));
        if (hierarchyContour[i][3] == -1) {
            if (contourArea(contours[i]) > param.objSizeMin_trkbar) {//sizeThreshold) {
                //rectangle(detectColorMask, boundRect[i].tl(), boundRect[i].br(), Scalar(0, 255, 0), 2, 8, 0);
                //drawContours(detectColorMask, contours, i, Scalar(0, 255, 255), 1, LINE_AA, hierarchyContour, std::abs(_levels));
                //drawContours(detMat, contours/*_poly*/, i, Scalar(255), /*2*/ CV_FILLED, LINE_AA, hierarchyContour, std::abs(_levels));
                //drawContours(detMat, colorFrame, i, Scalar(0,255,255), 2 /*CV_FILLED*/, LINE_AA, hierarchyContour, std::abs(_levels));
                //drawContours(test, contours, i, Scalar(0,255,0), CV_FILLED, LINE_AA, hierarchyContour, std::abs(_levels));
                drawContours(detectMask, contours/*_poly*/, i, Scalar(255), /*2*/ CV_FILLED, LINE_AA, hierarchyContour, std::abs(_levels));
                actorsContour.push_back(contours[i]); //contour of a detected object
                actorsRectangle.push_back(boundRect[i]);
                nbObj++;
          } 
        }
    }
    Mat RGBMask;
    cvtColor(detectMask, RGBMask, CV_GRAY2BGR);
    colorFrameIn.copyTo(detectionColorMask, RGBMask); // apply the mask to the image (src.copyTo(dst, mask))
    //imshow("contour", test);
}
