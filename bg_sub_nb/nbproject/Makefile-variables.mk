#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug=bg_sub_nb
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux-x86/bg_sub_nb
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug=bgsubnb.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux-x86/package/bgsubnb.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux-x86
CND_ARTIFACT_NAME_Release=bg_sub_nb
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux-x86/bg_sub_nb
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release=bgsubnb.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux-x86/package/bgsubnb.tar
# code_coverage configuration
CND_PLATFORM_code_coverage=GNU-Linux-x86
CND_ARTIFACT_DIR_code_coverage=dist/code_coverage/GNU-Linux-x86
CND_ARTIFACT_NAME_code_coverage=bg_sub_nb
CND_ARTIFACT_PATH_code_coverage=dist/code_coverage/GNU-Linux-x86/bg_sub_nb
CND_PACKAGE_DIR_code_coverage=dist/code_coverage/GNU-Linux-x86/package
CND_PACKAGE_NAME_code_coverage=bgsubnb.tar
CND_PACKAGE_PATH_code_coverage=dist/code_coverage/GNU-Linux-x86/package/bgsubnb.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
