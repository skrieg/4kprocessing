var struct_gui_param =
[
    [ "GuiParam", "struct_gui_param.html#a71743f4f9b098fb32ec4567a2660a3c4", null ],
    [ "blobDetectActive", "struct_gui_param.html#a0f45b442093bfb64ff7bd4dcd371d37a", null ],
    [ "blobSizeMax_trkbar", "struct_gui_param.html#a756a83aa53cd8fd75faebfe2bc85da7d", null ],
    [ "blobSizeMin_trkbar", "struct_gui_param.html#ad9b81150b71e70ecdb7971f9f7974252", null ],
    [ "canny_t1", "struct_gui_param.html#a26ceecaa897e44436e8b83f4fb437b42", null ],
    [ "canny_t2", "struct_gui_param.html#aac82f3b7dfc66f25fb931566988caf3f", null ],
    [ "closeActive", "struct_gui_param.html#ac211c8941a3303acc812080a26f49d1f", null ],
    [ "closElmt_trkbar", "struct_gui_param.html#ab3d7670c924e8a5fb6f7d85c3f2875f1", null ],
    [ "contPoly_trkbar", "struct_gui_param.html#a0a2e7acf1ff42423a4f0ee2819880fcc", null ],
    [ "DilateActive", "struct_gui_param.html#ae5f2f64ae7a91a224cbc7ba06ab09953", null ],
    [ "dilateElmt_trkbar", "struct_gui_param.html#a4649c3b25856210aa4291fbadc22fac7", null ],
    [ "gridFillActive", "struct_gui_param.html#a446291ec967340464c367820f7b14c59", null ],
    [ "history_trkbar", "struct_gui_param.html#afc337b28cce4b25ba40727d2a2b30d8f", null ],
    [ "holesFillActive", "struct_gui_param.html#a61d12a221d95ed80933a81856fc42b0f", null ],
    [ "hough_accu_tresh", "struct_gui_param.html#ab9631a7eb886acbe8f9a2957490d57d5", null ],
    [ "hough_line_min", "struct_gui_param.html#ad2a02daf910a3e88a93d102c78ba9c1e", null ],
    [ "hough_max_gap", "struct_gui_param.html#a6b85b5d1ef03153caf31a3a1460fe9f9", null ],
    [ "KnnDist2Threshold", "struct_gui_param.html#ac3779b7bb503ce4c9f07c41bb8378ff9", null ],
    [ "kNNSamples", "struct_gui_param.html#a5c3232332e1e3c7e3a258b91b48dae42", null ],
    [ "mixture_trkbar", "struct_gui_param.html#aacc5ce0ac2939977d6715ee6cfbf9ff1", null ],
    [ "Mog2Complexity_trkbar", "struct_gui_param.html#af60ac896e764d15bc6898a6b7daa0497", null ],
    [ "Mog2Ratio_trkbar", "struct_gui_param.html#a95a77f88613d0304ca295ab49b1017c0", null ],
    [ "Mog2Variance_trkbar", "struct_gui_param.html#a910f3c45502c79afb1fca5b330660a4e", null ],
    [ "objSizeMin_trkbar", "struct_gui_param.html#a0ec6aba30cfb61f5e540b2ca88f566dc", null ],
    [ "openActive", "struct_gui_param.html#a97502b2f7d13ade84f1b0ab178c871dc", null ],
    [ "opnElmt_trkbar", "struct_gui_param.html#ab62dd0299c91aede043a58f963225904", null ],
    [ "shadow_trkbar", "struct_gui_param.html#acc3c4d04ff70c913560d96ed6e7c4ba4", null ],
    [ "ShadowActive", "struct_gui_param.html#ab4b8513fd33011027a617e1a1d155dc9", null ],
    [ "test1_trkbar", "struct_gui_param.html#afa4a7923e855d4490d455e7f298b02e0", null ]
];