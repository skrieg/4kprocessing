var classification_8cpp =
[
    [ "actors_classification", "classification_8cpp.html#adfbe64dad19b33c7d7df6e35938d7e74", null ],
    [ "actors_classification_v2", "classification_8cpp.html#a0918e5b058308984d61b9d81060c5983", null ],
    [ "compute_score", "classification_8cpp.html#a49e008e905b09a53c47a9b32e1cd9280", null ],
    [ "getPos", "classification_8cpp.html#aeb49fc804e9cce03671b49c976cd6dcd", null ],
    [ "getRectCentroid", "classification_8cpp.html#a9d01eba4f46eb13f90a9ffbff314361e", null ],
    [ "isBounding", "classification_8cpp.html#a96bf8534c74bec413c6d93aa6158c567", null ],
    [ "isPresent", "classification_8cpp.html#a901f7fbbd7c4f122a7cb331f603d210d", null ]
];